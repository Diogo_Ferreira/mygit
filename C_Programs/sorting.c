#include<stdio.h>
#include<stdlib.h>

int soma(int conjunto[], int n)
{
    int resultado = 0;
    for(int i = 0; i < n; i++)
    {
        resultado = resultado + conjunto[i];
    }
    return resultado;
}

int seraPrimo(int n)
{
    int divisao;
    int primo = 1;

    for(int d = n-1; d > 1; d--)
    {
        divisao = (n % d);

        if(divisao == 0)
        {
            primo = 0;
            goto FIM;
        }
    }    
    
    FIM:
        return primo;
}

void sortC(int conjunto[], int n)
{
    int pmax;
    int max = conjunto[0];
    int conjunto_organizado[n];

    for(int x = (n-1); x >= 0; x--)
    {

        for(int i = 0; i < n; i++)
        {
            if(max <= conjunto[i])
            {
                max = conjunto[i];
                pmax = i;
            } 
        }

        conjunto[pmax] = 0;
        conjunto_organizado[x] = max;
        max = 0;
    }
    printf("\n \n");
    printf("O conjunto por ordem crescente é: ");
    for(int i = 0; i < n; i++)
    {
         printf("%d    ", conjunto_organizado[i]);
    }

}

void sortD(int conjunto[], int n)
{
    int pmax;
    int max = conjunto[0];
    int conjunto_organizado[n];

    for(int x = 0; x < n; x++)
    {

        for(int i = 0; i < n; i++)
        {
            if(max <= conjunto[i])
            {
                max = conjunto[i];
                pmax = i;
            } 
        }

        conjunto[pmax] = 0;
        conjunto_organizado[x] = max;
        max = 0;
    }
    printf("\n \n");
    printf("O conjunto por ordem decrescente é: ");
    for(int i = 0; i < n; i++)
    {
         printf("%d    ", conjunto_organizado[i]);
    }
}

int main()
{

    int x;
    int n;
    int opcao;
    int primo;
    int resultado;
    char sair;

    while(1){
    printf("Quantos números queres usar: ");
    scanf("%d", &n);
    printf("\n");

    int conjunto[n];
    int valor;
    printf("Insere os números:\n");
        
    for(int i = 0; i < n; i++)
        {
            scanf("%d", &valor);
            conjunto[i] = valor;
        }

    printf("\n");
    printf("---------------------------------------------------------------------\n");
    printf("Os valores inseridos foram: ");
        

    for(int i = 0; i < n; i++)
        {  
            printf("%d    ", conjunto[i]);  
        }
    printf("\n");
    printf("---------------------------------------------------------------------\n");
    printf("\n");

   
    printf("1 - Oredem crescente; \n");
    printf("2 - Oredem decrescente; \n");
    printf("3 - Soma de todos os números; \n");
    printf("4 - Descobrir os números primos; \n \n");
    printf("O que queres fazer: ");
    scanf("%d", &opcao);
    printf("\n \n");

    switch(opcao)
    {
        case 1:
            sortC(conjunto, n);
            break;
        case 2:
            sortD(conjunto, n);
            break;
        case 3:
            resultado = soma(conjunto, n);
            printf("%d", resultado);

            break;
        default:
            printf("Os números primos do teu conjunto são: ");
                
            for(int i = 0; i < n; i++)
            {
                x = conjunto[i];
                primo = seraPrimo(x);
                if(primo == 1)
                {
                    printf("%d   ", x);
                }
            }
        break;
        }  
        printf("\n \n \n");
        
    }
    return 0;
}