#include <stdlib.h>
#include <stdio.h>

int seraPrimo(int n)
{
    int divisao;
    int primo = 1;

    for (int d = n - 1; d > 1; d--)
    {
        divisao = (n % d);

        if (divisao == 0)
        {
            primo = 0;
            break;
        }
    }
}

void conjuntoPrimos(int n)
{
    int teste;
    while (n > 0)
    {
        teste = seraPrimo(n);
        if (teste != 0)
        {
            printf("%d  ", n);
        }

        n--;
    }
}

int main()
{
    int n;
    int primo;
    printf("Insira um número: ");
    scanf("%d", &n);
    printf("\n");
    primo = seraPrimo(n);

    switch (primo)
    {
    case 1:
        printf("O número é primo\n");
        break;
    default:
        printf("O número não é primo\n");
        break;
    }
    printf("-------------------------------------------------------\n");
    printf("O conjunto dos números primos até ao teu número são: \n");
    printf("\n");
    conjuntoPrimos(n);
    printf("\n");
    printf("\n");

    return 0;
}
