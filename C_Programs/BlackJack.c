#include<stdlib.h>
#include<stdio.h>
#include<time.h>

#ifdef _WIN32  //choosing sleeping lib based on OS
#include <Windows.h>
#else
#include <unistd.h>
#endif

#define clear() printf("\033[H\033[J")

char faces[13] = {'A', '2', '3', '4', '5', '6', '7', '8', '9', 'X', 'J', 'Q', 'K'};
char suits[4] = {'C', 'H', 'S', 'D'};
int value[13] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10};


void hasExploded(int playerPoints, int computerPoints)  //need to add it in the end to functions   hasExploded(playerPoints, computerPoints);
{
    if (playerPoints > 21)
    {
        printf("\n");
        printf("YOU EXPLODED!!! \n");
        exit(0);
    } 
    if (computerPoints > 21)
    {
         printf("You Won!!! \n");
         exit(0);
    }  
}


void player_has_3(int playerCard1, int ps1, int playerCard2, int ps2, int playerCard3, int ps3, int playerPoints)
{
    
    printf("\n");
    printf("Your cards: %c%c %c%c %c%c               Points: %d\n", faces[playerCard1], suits[ps1], faces[playerCard2],suits[ps2], faces[playerCard3], suits[ps3], playerPoints);
}

void player_has_4(int playerCard1, int ps1, int playerCard2, int ps2, int playerCard3, int ps3, int playerCard4, int ps4, int playerPoints)
{
    printf("\n");
    printf("Your cards: %c%c %c%c %c%c %c%c              Points: %d\n", faces[playerCard1], suits[ps1], faces[playerCard2],suits[ps2], faces[playerCard3], suits[ps3], faces[playerCard4], suits[ps4], playerPoints);
}

void player_has_5(int playerCard1, int ps1, int playerCard2, int ps2, int playerCard3, int ps3, int playerCard4, int ps4, int playerCard5, int ps5, int playerPoints)
{
    printf("\n");
    printf("Your cards: %c%c %c%c %c%c %c%c %c%c             Points: %d\n", faces[playerCard1], suits[ps1], faces[playerCard2],suits[ps2], faces[playerCard3], suits[ps3], faces[playerCard4], suits[ps4], faces[playerCard5], suits[ps5], playerPoints);
    if (playerPoints > 21)
    {
        printf("YOU EXPLODED!");
    } else
    {
        printf("YOU WON!");
    }  
}

void computer_has_2(int computerCard1, int cs1, int computerCard2, int cs2, int computerPoints, int playerPoints)
{
    printf("Computer cards: %c%c %c%c             Points: %d\n", faces[computerCard1], suits[cs1], faces[computerCard2], suits[cs2], computerPoints);
}

void computer_has_3(int computerCard1, int cs1, int computerCard2, int cs2, int computerCard3, int cs3, int computerPoints, int playerPoints)
{
    printf("Computer cards: %c%c %c%c %c%c            Points: %d\n", faces[computerCard1], suits[cs1], faces[computerCard2], suits[cs2], faces[computerCard3], suits[cs3], computerPoints);
}

void computer_has_4(int computerCard1, int cs1, int computerCard2, int cs2, int computerCard3, int cs3, int computerCard4, int cs4, int computerPoints, int playerPoints)
{
    printf("Computer cards: %c%c %c%c %c%c %c%c         Points: %d\n", faces[computerCard1], suits[cs1], faces[computerCard2], suits[cs2], faces[computerCard3], suits[cs3], faces[computerCard4], suits[cs4], computerPoints);

}



int main()
{
    int playerPoints, computerPoints, playerCard1, playerCard2, playerCard3, playerCard4, playerCard5, computerCard1, computerCard2, computerCard3, computerCard4, computerCard5, ps1, ps2, ps3, ps4, ps5, cs1, cs2, cs3, cs4, cs5;

    srand(time(0));
    clear();

    printf("\n");
    printf("Welcome to Blackjack! \n \n");
    printf("  -> In this game your goal is to get as close as you can to 21 points; \n");
    printf("  -> If your point count surpasses 21 you explode and loose; \n");
    printf("  -> If both players have less the 21 points the one with more points wins; \n");
    printf("  -> The first character will be the number/letter of your card and next to it will appear a letter that resembles the card's suit\n"); 
    printf("  -> Press 1 to get one more card and 0 to stop getting cards any other keys will count as the standing decision. \n \n \n");
    printf("Press Enter to start:");
    getchar();

    
    playerCard1 = rand()%12; ps1 = rand()%3;
    playerCard2 = rand()%12; ps2 = rand()%3;
    computerCard1 = rand()%12; cs1 = rand()%3;
     
    playerPoints = value[playerCard1] + value[playerCard2];
    computerPoints = value[computerCard1];

    int playerMove1;
    clear();
    printf("\n");
    printf("Your cards: %c%c %c%c                Points: %d\n", faces[playerCard1], suits[ps1], faces[playerCard2],suits[ps2], playerPoints);
    printf("Computer cards: %c%c               Points: %d \n\n", faces[computerCard1], suits[cs1], computerPoints);
    printf("What do you wanna do 1/0: ");
    scanf("%d", &playerMove1);

    if (playerMove1 == 1) //player hits (3)
    {
        int playerMove2;
        playerCard3 = rand()%12; ps3 = rand()%3;
        playerPoints += value[playerCard3];
        printf("\n");
        player_has_3(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerPoints);

        hasExploded(playerPoints, computerPoints);

        printf("What do you wanna do 1/0: ");
        scanf("%d", &playerMove2);

        if (playerMove2 == 1)  //player hits (4)
        {
            clear();
            int playerMove3;
            playerCard4 = rand()%12; ps4 = rand()%3;
            playerPoints += value[playerCard4];
            player_has_4(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerCard4, ps4, playerPoints);

            hasExploded(playerPoints, computerPoints);

            printf("What do you wanna do 1/0: ");
            scanf("%d", &playerMove3);

            if (playerMove3 == 1)  //player hits (5)
            {
                clear();
                playerCard5 = rand()%12; ps5 = rand()%3;
                playerPoints += value[playerCard5];
                player_has_5(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerCard4, ps4, playerCard5, ps5, playerPoints);

            }else //player stands (3)
            {
                clear();
                computerCard2 = rand()%12; cs2 = rand()%3;
                computerPoints += value[computerCard2];
                printf("\n");
                player_has_4(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerCard4, ps4, playerPoints);
                computer_has_2(computerCard1, cs1, computerCard2, cs2, computerPoints, playerPoints);

                if(computerPoints > playerPoints) //computer wins (2);
                {
                    printf("\n \n");
                    printf("You lost! \n");

                } else  //computer hits (3)
                {
                    clear();
                    computerCard3 = rand()%12; cs3 = rand()%3;
                    computerPoints += value[computerCard3];
                    printf("\n");
                    player_has_4(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerCard4, ps4, playerPoints);
                    computer_has_3(computerCard1, cs1, computerCard2, cs2,  computerCard3, cs3, computerPoints, playerPoints);

                    hasExploded(playerPoints, computerPoints);

                    if(computerPoints > playerPoints) //computer wins (3);
                    {
                        printf("\n \n");
                        printf("You lost! \n");
                    } else  //computer hits (4)
                    {
                        clear();
                        computerCard4 = rand()%12; cs4 = rand()%3;
                        computerPoints += value[computerCard4];
                        printf("\n");
                        player_has_4(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerCard4, ps4, playerPoints);
                        computer_has_4(computerCard1, cs1, computerCard2, cs2,  computerCard3, cs3, computerCard4, cs4, computerPoints, playerPoints);

                        hasExploded(playerPoints, computerPoints);

                        if(computerPoints > playerPoints) //computer wins (4);
                        {
                            printf("\n \n");
                            printf("You lost! \n");
                        } else
                        {
                            printf("\n \n");
                            printf("You won!\n");
                            exit(0);
                        }
                        
                    }
                }
            }
        } else // players stands (3)
        {
            clear();
            computerCard2 = rand()%12; cs2 = rand()%3;
            computerPoints += value[computerCard2];
            printf("\n");
            player_has_3(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerPoints);
            computer_has_2(computerCard1, cs1, computerCard2, cs2, computerPoints, playerPoints);

            hasExploded(playerPoints, computerPoints);

            if(computerPoints > playerPoints) //computer wins (2);
            {
                printf("\n \n");
                printf("You lost! \n");

            } else  //computer hits (3)
            {
                clear();
                computerCard3 = rand()%12; cs3 = rand()%3;
                computerPoints += value[computerCard3];
                printf("\n");
                player_has_3(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerPoints);
                computer_has_3(computerCard1, cs1, computerCard2, cs2,  computerCard3, cs3, computerPoints, playerPoints);

                hasExploded(playerPoints, computerPoints);

                if(computerPoints > playerPoints) //computer wins (3);
                {
                    printf("\n \n");
                    printf("You lost! \n");
                } else  //computer hits (4)
                {
                    clear();
                    computerCard4 = rand()%12; cs4 = rand()%3;
                    computerPoints += value[computerCard4];
                    printf("\n");
                    player_has_3(playerCard1, ps1, playerCard2, ps2, playerCard3, ps3, playerPoints);
                    computer_has_4(computerCard1, cs1, computerCard2, cs2,  computerCard3, cs3, computerCard4, cs4, computerPoints, playerPoints);

                    hasExploded(playerPoints, computerPoints);

                    if (computerPoints > playerPoints)
                    {
                        printf("\n \n");
                        printf("You lost! \n");
                    } else
                    {
                        printf("\n \n");
                        printf("You win! \n");
                    }
                }
            }
        }
        
    } else //player stands (2)
    {
        computerCard2 = rand()%12; cs2 = rand()%3;
        computerPoints += value[computerCard2];
        printf("\n");
        printf("Your cards: %c%c %c%c                Points: %d\n", faces[playerCard1], suits[ps1], faces[playerCard2],suits[ps2], playerPoints);
        computer_has_2(computerCard1, cs1, computerCard2, cs2, computerPoints, playerPoints);

        if(computerPoints > playerPoints) //computer wins (2);
        {
            printf("\n \n");
            printf("You lost! \n");

        } else  //computer hits (3)
        {
            clear();
            computerCard3 = rand()%12; cs3 = rand()%3;
            computerPoints += value[computerCard3];
            printf("\n");
            printf("Your cards: %c%c %c%c                Points: %d\n", faces[playerCard1], suits[ps1], faces[playerCard2],suits[ps2], playerPoints);
            computer_has_3(computerCard1, cs1, computerCard2, cs2,  computerCard3, cs3, computerPoints, playerPoints);

            hasExploded(playerPoints, computerPoints);


            if(computerPoints > playerPoints) //computer wins (3);
            {
                printf("\n \n");
                printf("You lost! \n");
            } else  //computer hits (4)
            {
                clear();
                computerCard4 = rand()%12; cs4 = rand()%3;
                computerPoints += value[computerCard4];
                printf("\n");
                printf("Your cards: %c%c %c%c                Points: %d\n", faces[playerCard1], suits[ps1], faces[playerCard2],suits[ps2], playerPoints);
                computer_has_4(computerCard1, cs1, computerCard2, cs2,  computerCard3, cs3, computerCard4, cs4, computerPoints, playerPoints);

                hasExploded(playerPoints, computerPoints);

                if (computerPoints > playerPoints)
                {
                    printf("\n \n");
                    printf("You lost! \n");
                } else
                {
                    printf("\n \n");
                    printf("You win! \n");
                }
            }
        }
    }
    
    return 0;
}