#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define stack_is_empty() stack_index == -1
#define stack_push() stack[++stack_index] = c
#define stack_pop() --stack_index

#define can_join() (stack[stack_index] + c == 'F' + 'W') || (stack[stack_index] + c == 'A' + 'E')

void process_string(int len)
{
    char c;
    char *stack = malloc(len * sizeof(char));
    register int stack_index = -1;
    register int i = 0;

    for (i = 0; i < len; ++i)
    {
        scanf("%c", &c);

        if (stack_is_empty())
        {
            stack_push();
        }
        else if (can_join())
        {
            stack_pop();
        }
        else
        {
            stack_push();
        }
    }

    printf("%*.*s\n", stack_index + 1, stack_index + 1, stack);
}

int main()
{
    int len;

    //* Input:
    scanf("%d", &len);
    if (len == 0)
    {
        printf("\n");
        return 0;
    }

    process_string(len);

    return 0;
}