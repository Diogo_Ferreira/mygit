#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define stack_is_empty() stack_index == 0
#define stack_push() stack[stack_index++] = str[i]
#define stack_pop() stack_index--

int can_join(char c1, char c2)
{
    if ((c1 == 'F' && c2 == 'W') || (c1 == 'W' && c2 == 'F'))
        return 1;
    else if ((c1 == 'E' && c2 == 'A') || (c1 == 'A' && c2 == 'E'))
        return 1;
    else
        return 0;
}

void process_string(int len, char *str)
{
    char *stack = malloc(len * sizeof(char));
    int stack_index = 0;

    for (int i = 0; i < len; i++)
    {
        if (stack_is_empty())
            stack_push();

        else if (can_join(stack[stack_index - 1], str[i]))
            stack_pop();

        else
            stack_push();
    }

    printf("%*.*s\n", stack_index, stack_index, stack);
    free(stack);
}

int main()
{
    char *input;
    int len;

    //* Input:
    scanf("%d", &len);
    if (len == 0)
    {
        printf("\n");
        return 0;
    }
    input = malloc(len * sizeof(char));
    scanf("%s", input);

    process_string(len, input);

    free(input);
    return 0;
}