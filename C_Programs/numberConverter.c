#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define clear() printf("\033[H\033[J")

void dec_to_bin(long number);
void dec_to_hex(long number);
int hexa_letters(long number);
int bin_to_dec(long number);


int main()
{
        int choice, decimal, digits = 0;
    long number;
    char hexa[digits];

    clear();
    while (1)
    {  
        BEG:  
        printf("Choose a base: \n \n");
        printf("    1 - Binary \n");
        printf("    2 - Decimal \n");
        printf("    3 - Quit \n");
        printf("\n");

        printf("Option: ");
        scanf("%d", &choice);
        
        clear();

        switch (choice)
        {
        case 1:
            printf("Write your binary number: ");
            scanf("%lu", &number);
            printf("\n");
            decimal = bin_to_dec(number);
            printf("Your number in decimal is: %d \n \n", decimal);
            dec_to_hex(decimal);
            break;
        case 2:
            printf("Write your number: ");
            scanf("%lu", &number);
            printf("\n");
            dec_to_bin(number);
            dec_to_hex(number);
            printf("\n");
            break;
        case 3:
            exit(0);
            break;
        default:
            printf("Invalid option, try again!!! \n \n");
            goto BEG; 
            break;
        }
    }
    return 0;
}

int bin_to_dec(long number)
{
    int decimal = 0, x, base = 1;
    
    while (number > 0)
    {
        x = number % 10;
        decimal += (x * base);
        number /= 10 ;
        base *= 2;
    }
    return decimal;
}

void dec_to_bin(long number)
{
    int bin[1000], digits = -1, space;

    for(int i = 0; number > 0 ; i++)
    {
        bin[i] = number % 2;
        number /= 2;
        digits ++;        
    }

    space = 4 - ((digits + 1) % 4);

    printf("Your number in binary is: ");

    for (int i = digits; i >= 0; i--)
    {
        printf("%d", bin[i]);

        space ++;
        if (space % 4 == 0)   // Number spacing controll
        {
            printf(" ");
        }
    }
    printf("\n \n");
}

int hexa_letters(long n)
{

    int letter[6] = {'A', 'B', 'C', 'D', 'E', 'F'};
    n -= 10;
    return letter[n];
}

void dec_to_hex(long number)
{
    int hex[1000], digits = -1, space, test;

    for(int i = 0; number > 0 ; i++)
    {
        test = number % 16;
        if(test < 10)     //if digit is a number
        {
            hex[i] = number % 16;
            number /= 16;
        } else                     //if digit is a letter
        {
            hex[i] = hexa_letters(test);
            number /= 16;
        }      
        digits ++;        
    }

    space = 4 - ((digits + 1) % 4);

    printf("Your number in hexadecimal is: ");

    for (int i = digits; i >= 0; i--)
    {
        if (hex[i] < 10)
        {
            printf("%d", hex[i]);
        } else
        {        
            printf("%c", hex[i]);
        }

        space ++;
        if (space % 4 == 0)   // Number spacing controll
        {
            printf(" ");
        }
    }
    printf("\n");
}


