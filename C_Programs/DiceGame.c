#include<stdlib.h>
#include<stdio.h>
#include<time.h>

#ifdef _WIN32  //choosing sleeping lib based on OS
#include <Windows.h>
#else
#include <unistd.h>
#endif

#define clear() printf("\033[H\033[J")

void diceFaces(int dice1, int dice2)
{
    switch(dice1)
    {
    case 1:
        printf("   ___________    \n");    
        printf("  |           |   \n");
        printf("  |           |    \n");
        printf("  |     *     |   \n");
        printf("  |           |   \n");
        printf("  |___________|  \n \n");       
        break;
    case 2:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *        |    \n");
        printf("  |           |   \n");
        printf("  |        *  |   \n");
        printf("  |___________|  \n \n");
        break;
    case 3:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *        |    \n");
        printf("  |     *     |   \n");
        printf("  |        *  |   \n");
        printf("  |___________|  \n \n");
        break;
    case 4:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |   \n");
        printf("  |___________|  \n \n");
        break;
    case 5:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |    \n");
        printf("  |     *     |   \n");
        printf("  |  *     *  |   \n");
        printf("  |___________|   \n \n");
        break;
    default:   //6
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |    \n");
        printf("  |  *     *  |   \n");
        printf("  |  *     *  |   \n");
        printf("  |___________|   \n \n");
        break;
    }
    switch(dice2)
    {
    case 1:
        printf("   ___________    \n");    
        printf("  |           |   \n");
        printf("  |           |    \n");
        printf("  |     *     |   \n");
        printf("  |           |   \n");
        printf("  |___________|  \n \n");       
        break;
    case 2:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *        |    \n");
        printf("  |           |   \n");
        printf("  |        *  |   \n");
        printf("  |___________|  \n \n");
        break;
    case 3:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *        |    \n");
        printf("  |     *     |   \n");
        printf("  |        *  |   \n");
        printf("  |___________|  \n \n");
        break;
    case 4:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |   \n");
        printf("  |___________|  \n \n");
        break;
    case 5:
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |    \n");
        printf("  |     *     |   \n");
        printf("  |  *     *  |   \n");
        printf("  |___________|   \n \n");
        break;
    default:   //6
        printf("   ___________    \n");
        printf("  |           |   \n");
        printf("  |  *     *  |    \n");
        printf("  |  *     *  |   \n");
        printf("  |  *     *  |   \n");
        printf("  |___________|   \n \n");
        break;
    }
    int sum = dice1 + dice2;    
}

int evenOdd(int dice1, int dice2, int betMoney)
{
    clear();
    printf("Even or Odd: \n \n");
    int choice;
    int sum = dice1 + dice2;
    int odd = sum % 2; //if 1 odd
    printf("Press 0 for even and 1 for odd: ");
    scanf("%d", &choice);

    if (odd == choice)
    {
        diceFaces(dice1, dice2);
        betMoney *= 2;
        printf("You won: +%d$ \n", betMoney / 2);
        sleep(4);        
    } else
    {
        diceFaces(dice1, dice2);
        
        printf("You Lost: -%d$ \n", betMoney);
        betMoney = 0;
        sleep(4);
    }
    return betMoney;
}

int equalFaces(int dice1, int dice2, int betMoney)
{
    clear();
    printf("Equal dice faces: \n \n");
    diceFaces(dice1, dice2);

    if (dice1 == dice2)
    {
        betMoney *= 6;
        printf("You won: +%d$ \n", betMoney / 2);
        sleep(4);           
    } else
    {
        printf("You Lost: -%d$ \n", betMoney);
        betMoney = 0;
        sleep(4);
    }
    return betMoney;
}

int guess1Number(int dice1, int dice2, int betMoney)
{
    clear();
    int choice;
    printf("Guess one number: \n \n");
    printf("Your guess: ");
    scanf("%d", &choice);
    diceFaces(dice1, dice2);

    if (choice == dice1 || choice == dice2)
    {
        betMoney *= 6;
        printf("You won: +%d$ \n", betMoney / 2);
        sleep(4);     
    } else
    {
        printf("You Lost: -%d$ \n", betMoney);
        betMoney = 0;
        sleep(4); 
    }
    return betMoney;
}

int guessSum(int dice1, int dice2, int betMoney)
{
    clear();
    int choice, sum;
    sum = dice1 + dice2;
    printf("Guess sum: \n \n");
    printf("Your guess: ");
    scanf("%d", &choice);
    diceFaces(dice1, dice2);

    if (choice == sum)
    {
        betMoney *= 12;
        printf("You won: +%d$ \n", betMoney / 2);
        sleep(4);     
    } else
    {
        printf("You Lost: -%d$ \n", betMoney);
        betMoney = 0;
        sleep(4); 
    }
    return betMoney;
}

int main()
{
    srand(time(0));
    int dice1, dice2, money, betTipe, betMoney;
    money = 100;
    
    clear();
    printf("Welocome to my dice game! \n \n \n");
    printf("You will start with 100$ and you can either loose or earn money based on your bets. \n \n \n");
    printf("Press Enter to start:");
    getchar();
    while (money > 0)
    {
        clear();
        printf("Bets: \n \n");
        printf("1-> Even/Odd - 2x money;  \n");
        printf("2-> Equal dice faces - 6x money; \n");
        printf("3-> Guessing one of the dice faces - 6x money; \n");
        printf("4-> Guessing the sum of both dices - 12x money; \n \n");
        printf("Money:  %d$ \n \n", money);
        dice1 = rand()%5 +1;
        dice2 = rand()%5 +1;

        printf("Betting money: ");
        scanf("%d", &betMoney);

        while (betMoney > money)  //Bet validation
        {  
        printf("Chose a bet that you can afford: ");
        scanf("%d", &betMoney);
        }
        
        money -= betMoney;
        printf("Choose the bet type: ");
        scanf("%d", &betTipe);

        switch(betTipe)
        {
        case 1:
            betMoney = evenOdd(dice1, dice2, betMoney);
            money += betMoney;
            break;
        case 2:
            betMoney = equalFaces(dice1, dice2, betMoney);
            money += betMoney;
            break;
        case 3:
            betMoney = guess1Number(dice1, dice2, betMoney);
            money += betMoney;
            break;
        default:
            betMoney = guessSum(dice1, dice2, betMoney);
            money += betMoney;
            break;
        }
    }
    printf("\n \n");
    printf("GAME OVER... \n \n");
}