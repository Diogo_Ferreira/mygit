#ifndef STRUCT
#define STRUCT

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

enum columns
{
    Confirmados,
    Recuperados,
    Obitos
};

typedef struct info
{
    int date[3];
    int values[3];
    struct info *next;
} INFO;

#endif