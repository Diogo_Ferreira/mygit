#include "io.h"
#include "dataManager.h"

INFO *read_file()
{
    INFO *head = NULL;
    bool first_line = true;
    char input[STR];

    FILE *csvPtr = fopen("covid_data.csv", "r");

    if (csvPtr != NULL)
    {
        rewind(csvPtr);

        while (fgets(input, STR, csvPtr) != NULL) //Reading one line
        {
            if (!first_line) //Ignoring first line
            {
                head = add_node(head, input);
            }
            else
            {
                first_line = false;
            }
        }
        fclose(csvPtr);
    }
    else
    {
        printf("Unabble to read file");
        exit(-1);
    }
    return head;
}
void print_all(INFO *head)
{
    FILE *pToFile = fopen("Final.csv", "w");

    rewind(pToFile);
    fprintf(pToFile, "Data,Confirmados,Recuperados,Óbitos\n");

    for (INFO *ptr = head; ptr != NULL; ptr = ptr->next)
    {
        fprintf(pToFile, "%d/%d/%d,%d,%d,%d \n", ptr->date[0], ptr->date[1], ptr->date[2], ptr->values[0], ptr->values[1], ptr->values[2]);
    }
    fclose(pToFile);
}