#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dataManager.h"
#include "struct.h"
#include "io.h"

int main()
{
    INFO *head = NULL;

    head = read_file();
    print_all(head);
    clean_all(head);

    return 0;
}