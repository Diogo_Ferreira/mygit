#include "dataManager.h"

INFO *add_node(INFO *head, char *input)
{
    INFO *new = (INFO *)calloc(1, sizeof(INFO));
    assign_values(new, input);

    if (head == NULL)
    {
        head = new;
        new->next = NULL;
    }
    else
    {
        new->next = head;
        head = new;
    }

    return head;
}

void assign_values(INFO *new, char *input)
{
    int length;
    int n;
    char copyInput[STR];
    char *token, *token2;

    length = strlen(input);
    strcpy(copyInput, input);

    // String cleaning
    if (input[length] == '\n')
    {
        input[length - 1] = '\0';
    }
    else if (input[length - 2] == '\r' && input[length - 1] == '\n')
    {
        input[length - 2] = '\0';
        input[length - 1] = '\0';
    }

    //Date
    token = strsep(&input, ",");

    for (int i = 0; i < 3; i++)
    {
        token2 = strsep(&token, "/");
        n = atoi(token2);

        new->date[i] = n;
    }

    for (int i = 0; i < 6; i++)
    {
        token = strsep(&input, ",");

        if (i > 2)
        {
            n = atoi(token);
            new->values[i - 3] = n;
        }
    }
}

void clean_all(INFO *head)
{
    INFO *aux;
    int i = 0;

    if (head != NULL)
    {
        for (aux = head; head != NULL; aux = head)
        {
            head = aux->next;
            free(aux);
            i++;
        }
    }
    printf("Cleaned - %d elements;", i);
}
