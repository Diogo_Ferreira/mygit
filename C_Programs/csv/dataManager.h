#ifndef DM
#define DM

#include "struct.h"

#define STR 100

INFO *add_node(INFO *head, char *input);
void assign_values(INFO *new, char *input);
void clean_all(INFO *head);

#endif