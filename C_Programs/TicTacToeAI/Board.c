#include "Board.h"

void printBoard(int *ptr)
{
    char pices[9];

    //Converting board values into chars to print them
    for (int i = 0; i < 9; ptr++, i++)
    {
        if (*ptr == 1)
        {
            pices[i] = 'X';
        }
        else if (*ptr == -1)
        {
            pices[i] = 'O';
        }
        else
        {
            pices[i] = ' ';
        }
    }

    printf(" \n \
A    %c  |  %c  |  %c  \n\
    - - -|- - -|- - -  \n \
B    %c  |  %c  |  %c  \n\
    - - -|- - -|- - -  \n \
C    %c  |  %c  |  %c  \n\n ",
           pices[0], pices[1], pices[2], pices[3], pices[4], pices[5], pices[6], pices[7], pices[8]);
    printf("     1     2     3 \n\n");
}

int coordinates(char str[2]) // function that will change string (ex: A2) to pointer to board position
{
    int line = str[0] - 'A';
    int column = str[1] - '1';

    return (line * 3 + column);
}

