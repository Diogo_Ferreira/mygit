#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>
#include "Board.h"
#include "gameManager.h"
#include "players.h"

#define clear() printf("\033[H\033[J") // clear screan macro

int main()
{
    int gameState = 0;
    int coordinate;
    int board[3][3] = {0};
    int *board_ptr = &board[0][0];

    srand(time(0));

    clear();

    //GAME EVENTS:
    while (gameState == 0)
    {
        //player 1 plays:
        printBoard(&board[0][0]);
        coordinate = player_move(&board[0][0], 1);
        *(board_ptr + coordinate) = 1;
        gameState = game_has_ended(board);

        if (gameState != 0)
        {
            break;
        } //finish loop case game ends

        //player 2 plays:
        printBoard(&board[0][0]);
        coordinate = player_move(&board[0][0], 2);
        *(board_ptr + coordinate) = -1;
        gameState = game_has_ended(board);
    }

    // End Game:
    printBoard(&board[0][0]);
    if (gameState == 1)
    {
        printf("\n\n Tie!\n\n");
    }
    else if (gameState == 3)
    {
        printf("\n\nX wins!!!\n\n");
    }
    else if (gameState == -3)
    {
        printf("\n\nO wins!!!\n\n");
    }
    else
    {
        printf("\n\nNobody wins!\n\n");
    }

    return 0;
}
