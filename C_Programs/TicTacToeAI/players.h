#ifndef PLAYER
#define PLAYER

#include <stdlib.h>
#include <stdio.h>
#include "Board.h"

int selector(int *board, int player, int ai);
int player_move(int *board, int player);
int ai_l1(int *board, int player);

#endif