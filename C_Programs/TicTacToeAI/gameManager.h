#ifndef GAME_MANAGER
#define GAME_MANAGER

#include<stdlib.h>
#include<stdio.h>
#include"Board.h"

int game_has_ended(int board[3][3]);

#endif