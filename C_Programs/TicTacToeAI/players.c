#include "players.h"
#include "time.h"

#define clear() printf("\033[H\033[J") // clear screan macro

int player_move(int *board, int player)
{
    char str[2];
    int coordinate;

    do // check for unvalid positioning
    {
        printf("P%d - Insert your placement coordinates (ex: A3): ", player);
        scanf("%s", str);
        coordinate = coordinates(str);
    } while (*(board + coordinate) != 0);
    clear();
    return coordinate;
}

int ai_l1(int *board, int player)
{
    int coordinate;

    do // check for unvalid positioning
    {
        coordinate = rand() % 9;
    } while (*(board + coordinate) != 0);
    clear();
    return coordinate;
}