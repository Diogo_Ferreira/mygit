#include "gameManager.h"

int game_has_ended(int board[3][3]) // function that will return 3 if(X wins); -3 if(O wins) 1 if(tie) and 0 if(the game didn't end)
{
    int control;
    /*      
               c1  c2  c3
            l1 00  01  02
            l2 10  11  12
            l3 20  21  22 
    */

    // Checking for a winner
    control = board[0][0] + board[1][1] + board[2][2]; //diagonal win
    if (control == 3 || control == -3)
    {
        return control;
    };

    control = board[0][2] + board[1][1] + board[2][0]; // diagonal win 2
    if (control == 3 || control == -3)
    {
        return control;
    };

    control = board[0][0] + board[1][0] + board[2][0]; // vertical win c1
    if (control == 3 || control == -3)
    {
        return control;
    };

    control = board[0][1] + board[1][1] + board[2][1]; // vertical win c2
    if (control == 3 || control == -3)
    {
        return control;
    };

    control = board[0][2] + board[1][2] + board[2][2]; // vertical win c3
    if (control == 3 || control == -3)
    {
        return control;
    };

    control = board[0][0] + board[0][1] + board[0][2]; // horizontal win l1
    if (control == 3 || control == -3)
    {
        return control;
    };

    control = board[1][0] + board[1][1] + board[1][2]; // horizontal win l2
    if (control == 3 || control == -3)
    {
        return control;
    };

    control = board[2][0] + board[2][1] + board[2][2]; // horizomntal win l3
    if (control == 3 || control == -3)
    {
        return control;
    };

    // Checking for tie or unfinished game
    control = 1;
    for (int line = 0; line < 3; line++)
    {
        for (int column = 0; column < 3; column++)
        {
            control *= board[line][column];
        }
    }

    // if (control == 0) unfinished game; else tie
    if (control == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
