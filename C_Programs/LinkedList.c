#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32 //choosing sleeping lib based on OS
#include <Windows.h>
#else
#include <unistd.h>
#endif

#define MAX 60
#define LINE 100
#define clear() printf("\033[H\033[J")

struct student
{
    int number;
    char name[MAX];
    char major[MAX];
    struct student *next;
};
typedef struct student STUDENT;
typedef STUDENT *PSTUDENT;

PSTUDENT head = NULL;

void insertBeginning(PSTUDENT new)
{
    new->next = head; // setting next pointer to head of previous member
    head = new;       // head points to the new student
}

// * This Function creates a student that can be inserted in the list with the help of other functions *
PSTUDENT createStudent()
{
    PSTUDENT new;
    char s[LINE];

    new = (PSTUDENT)malloc(sizeof(STUDENT));
    printf("\nStudent number: ");
    fgets(s, LINE, stdin);
    sscanf(s, "%d", &new->number);
    printf("Student name: ");
    fflush(stdout); // making shure it prints
    fgets(new->name, MAX, stdin);
    new->name[strlen(new->name) - 1] = '\0'; // ending string
    printf("Student major: ");
    fflush(stdout); // making shure it prints
    fgets(new->major, MAX, stdin);
    new->major[strlen(new->major) - 1] = '\0'; // ending string
    new->next = NULL;                          // ! setting next pointer to NULL
    return new;                                // returning pointer to new sudent
}

void print()
{
    PSTUDENT ptr; // ptr is a pointer to student struct
    printf("\n\n");
    for (ptr = head; ptr != NULL; ptr = ptr->next) // printing all list members
    {
        printf("   * %s - %d - %s\n", ptr->major, ptr->number, ptr->name);
    }
}

// * Function to clean list *
void CleanAll()
{
    PSTUDENT ptr = head;
    int n = 0;

    do
    {
        head = ptr->next;
        free(ptr);
        ptr = head;
        n++;
    } while (ptr != NULL);

    printf("Cleaned %d students \n", n);
    sleep(2);
}

void search_by_major(char major[MAX])
{
    PSTUDENT ptr;
    int studentsFound = 0;
    printf("Students with %s Major: \n", major);

    ptr = head;

    for (ptr = head; ptr != NULL; ptr = ptr->next) // printing all list members
    {
        if (strcmp(major, ptr->major) == 0)
        {
            printf("   * %s - %d - %s\n", ptr->major, ptr->number, ptr->name);
            studentsFound++;
        }
    }
    if (studentsFound == 0)
        printf("No students found");

    sleep(8);
}

void editStudent(int sNumber)
{
    PSTUDENT ptr;
    char s[LINE];
    int foundS = 0;

    for (ptr = head; ptr != NULL; ptr = ptr->next) // printing all list members
    {
        if (ptr->number == sNumber)
        {
            printf("Edit student: \n");
            printf("   * %s - %d - %s\n", ptr->major, ptr->number, ptr->name);

            printf("\nStudent number: ");
            fgets(s, LINE, stdin);
            sscanf(s, "%d", &ptr->number);
            printf("Student name: ");
            fflush(stdout); // making shure it prints
            fgets(ptr->name, MAX, stdin);
            ptr->name[strlen(ptr->name) - 1] = '\0'; // ending string
            printf("Student major: ");
            fflush(stdout); // making shure it prints
            fgets(ptr->major, MAX, stdin);
            ptr->major[strlen(ptr->major) - 1] = '\0';
            foundS++;
        }
    }
    if (foundS == 0)
    {
        printf("Student not found!");
        fflush(stdout);
    }
}

int main()
{
    PSTUDENT ptr;
    char input[10], aux, major[MAX];
    int option;

    while (1)
    {
        clear();
        printf("\
        \n  ________________________________ \
        \n |                                |\
        \n |   1 - Create new Student;      |\
        \n |   2 - Print List;              |\
        \n |   3 - Search Studet by Major;  |\
        \n |   4 - Edit Studet;             |\
        \n |   5 - Clean List;              |\
        \n |   6 - Quit;                    |\
        \n |________________________________|\n\n\n");

        printf("Option: ");
        fgets(input, 10, stdin);
        sscanf(input, "%d", &option);

        switch (option)
        {
        case 1:
            ptr = createStudent();
            insertBeginning(ptr);
            printf("\nStudent created!");
            fflush(stdout);
            sleep(1);
            break;
        case 2:
            printf("\n\n\n*- - - - - - - - - - - -*\n\n");
            printf("    List:");
            print();
            printf("\n*- - - - - - - - - - - -*\n\n");
            printf("Press any key to continue:");
            scanf("%c", &aux);
            break;
        case 3:
            printf("\n\nStudent major: ");
            fgets(major, MAX, stdin);
            major[strlen(major) - 1] = '\0';
            search_by_major(major);
            break;
        case 4:
            printf("\n\nStudent number: ");
            scanf("%d", &option);
            fflush(stdin);
            editStudent(option);
            break;
        case 5:
            CleanAll();
            break;
        default:
            CleanAll();
            exit(0);
            break;
        }
    }
    CleanAll();
    return 0;
}
