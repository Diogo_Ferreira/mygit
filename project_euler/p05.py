def divisible(n1, n2, numb):
    for i in [12, 13, 14, 16, 17, 18, 19]:
        if numb % i != 0:
            return False

    return True


x = 1000

while True:
    print(x)
    if divisible(2, 10, x):
        print("sol:", x)
        break
    x += 20
