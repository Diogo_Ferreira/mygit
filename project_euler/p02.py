fib = [1, 2]

sum = 0

i = 2
while True:
    new = fib[i - 1] + fib[i - 2]

    if new < 4000000:
        fib.append(new)
    else:
        break
    i += 1

for value in fib:
    if value % 2 == 0:
        sum += value

print(sum)
