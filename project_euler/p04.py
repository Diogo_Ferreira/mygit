max = 0


def is_palindrome(numb):
    x = str(numb)
    j = len(x) - 1
    for i in range((j // 2) + 1):
        if x[i] != x[j - i]:
            return False

    return True


for i in range(1000):
    for j in range(1000):
        n = i * j

        if is_palindrome(n) and n > max:
            max = n

print(max)
