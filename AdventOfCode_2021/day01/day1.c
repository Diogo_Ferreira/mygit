#include <stdio.h>
#include <stdlib.h>
#define LINES 2000

// Count the number of increses
void p1(FILE *fp)
{
	int count = 0;
	int prev = 0, current = 0;

	rewind(fp);
	fscanf(fp, "%d", &current);

	while (!feof(fp))
	{
		prev = current;
		fscanf(fp, "%d", &current);

		if (current > prev)
			count++;
	}
	printf("%d\n", count);
}

// Sum of three
void p2(FILE *fp)
{
	int count = 0;
	int buffer[4];

	rewind(fp);
	for (int i = 0; i < 4 && !feof(fp); i++)
		fscanf(fp, "%d", &buffer[i]);

	for (int l = 4; l <= LINES; l++)
	{
		// printf("%d %d %d %d\n", buffer[0], buffer[1], buffer[2], buffer[3]);
		if (buffer[1] + buffer[2] + buffer[3] > buffer[0] + buffer[1] + buffer[2])
		{
			count++;
			// printf("Added\n\n");
		}
		for (int i = 0; i < 3; i++)
			buffer[i] = buffer[i + 1];

		fscanf(fp, "%d", &buffer[3]);
	}
	printf("%d\n", count);
}

int main()
{
	FILE *fp = fopen("input.txt", "r");
	p1(fp);
	p2(fp);

	return 0;
}
