F = open("input.txt", 'r')


def filter_input(input):
    input = input.replace("->", "")
    input = input.replace(",", " ")
    input = input.split()

    return (int(input[0]), int(input[1]), int(input[2]), int(input[3]))


def printMap(map):
    for i in range(10):
        for j in range(10):
            print(map[i][j], "", end="")
        print()


def is_horizontal(cords):
    x1, y1, x2, y2 = cords

    if (x1 == x2) or (y1 == y2):
        return True
    else:
        return False


def make_diagonal(map, cords):
    x1, y1, x2, y2 = cords

    hMove = 1
    vMove = 1
    distance = abs(x2 - x1)

    if x1 > x2:
        hMove = -1
    if y1 > y2:
        vMove = -1

    for i in range(0, distance+1):
        map[y1 + i * vMove][x1 + i * hMove] += 1


def make_line(map, cords):

    if (not is_horizontal(cords)):
        make_diagonal(map, cords)
        return

    x1, y1, x2, y2 = cords

    if x1 > x2 or y1 > y2:
        x2, y2, x1, y1 = cords

    map[y1][x1] += 1
    map[y2][x2] += 1

    for i in range(x1+1, x2):
        map[y1][i] += 1
    for i in range(y1+1, y2):
        map[i][x1] += 1


def read_file(map):
    for line in F:
        make_line(map, filter_input(line))


def count(map):
    n = 0
    for line in map:
        for x in line:
            if x > 1:
                n += 1
    return n


def main():

    map = []
    for i in range(1000):
        line = [int(0)for i in range(1000)]
        map.append(line)

    read_file(map)
    print(count(map))


main()
F.close()
