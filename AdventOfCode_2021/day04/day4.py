# SCORE: sum of board unmaked numbers * last number drawn
F = open("input.txt", 'r')


class Board():
    def __init__(self):
        self.finished = False

    def print(self):
        for i in self.board:
            print(i)

        print()

    def add_line(self, line):
        l = line.split()
        l = [int(i) for i in l]
        self.board.append(l)

    def mark_value(self, n):
        for i in range(5):
            for j in range(5):
                if self.board[i][j] == n:
                    self.board[i][j] = -1

    def bingo(self):
        for i in range(5):
            if self.board[i][0] < 0 and self.board[i][1] < 0 and self.board[i][2] < 0 and self.board[i][3] and self.board[i][4] < 0:
                return True

        for i in range(5):
            if self.board[0][i] < 0 and self.board[1][i] < 0 and self.board[2][i] < 0 and self.board[3][i] and self.board[4][i] < 0:
                return True

        return False

    def get_score(self, n):
        score = 0

        for i in range(5):
            for j in range(5):
                if self.board[i][j] > 0:
                    score += self.board[i][j]
        return (score * n)


def read_file(F, boards,):
    i = -1

    for line in F:
        if line == '\n':
            i += 1
            x = Board()
            x.board = []
            boards.append(x)
        else:
            boards[i].add_line(line)
    return i


def p1(boards, numbers):

    for n in numbers:
        for x in boards:
            x.mark_value(n)
            if(x.bingo()):
                return x.get_score(n)

    return 0


def p2(boards, numbers):
    last = 0
    lastN = 0

    for n in numbers:
        index = -1
        for x in boards:
            index += 1

            if x.finished == True:
                continue

            x.mark_value(n)
            if(x.bingo()):
                x.finished = True
                last = index
                lastDrawn = n

    print("Board: ", last, "N - ", lastDrawn)
    boards[last].print()
    return boards[last].get_score(lastDrawn)


def main():
    numbers = F.readline()
    numbers = numbers.split(',')
    numbers = [int(i) for i in numbers]

    boards = []
    N = read_file(F, boards)

    #print("P1: ", p1(boards, numbers))
    print("P2: ", p2(boards, numbers))


main()
F.close()
