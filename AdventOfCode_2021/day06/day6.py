F = open("input.txt", 'r')


def read_file():
    fish = [int(0) for i in range(10)]

    line = F.readline()
    l = line.replace(",", " ")
    l = l.split()

    for x in l:
        fish[int(x)] += 1
    return fish


def next_day(fish):
    temp = fish[0]

    for i in range(1, 10):
        fish[i-1] = fish[i]

    fish[6] += temp
    fish[8] += temp

    return fish


def count(fish):
    days = 256
    n = 0

    for i in range(days):
        fish = next_day(fish)

    for i in range(10):
        n += fish[i]

    return n


def main():
    fish = read_file()
    print(count(fish))


main()
F.close()
