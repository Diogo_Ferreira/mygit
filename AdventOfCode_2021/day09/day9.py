def read_file():
    F = open("input.txt", "r")
    map = []

    for line in F:
        input = []
        for c in line:
            if c == "\n":
                continue
            input.append(int(c))
        map.append(input)

    F.close()
    return map


def get_neighbors(map, center):
    l, c = center
    neighbors = []

    # Up:
    if (l-1) >= 0:
        neighbors.append((l-1, c))
    # Down:
    if (l+1) < len(map):
        neighbors.append((l+1, c))
    # Left:
    if (c-1) >= 0:
        neighbors.append((l, c-1))
    # Right:
    if (c+1) < len(map[0]):
        neighbors.append((l, c+1))

    return neighbors


def local_min(map, center):
    l, c = center

    # Up:
    if (l-1) >= 0:
        if map[l][c] >= map[l-1][c]:
            return False
    # Down:
    if (l+1) < len(map):
        if map[l][c] >= map[l+1][c]:
            return False
    # Left:
    if (c-1) >= 0:
        if map[l][c] >= map[l][c-1]:
            return False
    # Right:
    if (c+1) < len(map[0]):
        if map[l][c] >= map[l][c+1]:
            return False

    return True


def p1(map):
    risk = 0
    mins = []

    for i in range(len(map)):
        for j in range(len(map[i])):
            if local_min(map, (i, j)):
                risk += map[i][j] + 1
                mins.append((i, j))

    print("Risk: ", risk)
    return mins


def bfs(map, cords):
    size = 1
    fifo = [x for x in get_neighbors(map, cords)]
    l, c = cords
    map[l][c] *= -1

    while len(fifo) > 0:
        i, j = fifo.pop(0)

        if map[l][c] < map[i][j] and map[i][j] != 9:
            map[i][j] *= -1
            size += 1

            for x in get_neighbors(map, (i, j)):
                fifo.append(x)

    return size


def p2(map, mins):
    basinSizes = []

    for x in mins:
        size = bfs(map, x)
        basinSizes.append(size)

    basinSizes.sort()
    i = len(basinSizes)
    print("P2: ", basinSizes[i-1]*basinSizes[i-2]*basinSizes[i-3])


def main():
    map = read_file()

    mins = p1(map)
    p2(map, mins)

    # <<<())))


main()
