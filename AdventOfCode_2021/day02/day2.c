#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct submarine
{
    int depth, horizontal, aim;
};
typedef struct submarine SUBMARINE;

// Count the number of increses
void p1(FILE *fp)
{
    int value;
    char direction[100];

    rewind(fp);

    SUBMARINE submarine;
    submarine.depth = 0;
    submarine.horizontal = 0;

    while (!feof(fp))
    {
        fscanf(fp, "%s %d", direction, &value);

        if (strcmp(direction, "forward") == 0)
            submarine.horizontal += value;
        else if (strcmp(direction, "down") == 0)
            submarine.depth += value;
        else if (strcmp(direction, "up") == 0)
            submarine.depth -= value;
    }
    printf("p1 - %d\n", submarine.horizontal * submarine.depth);
}

/*
* down (positive) X increases your aim by X units.
* up X decreases your aim by X units.

* forward X does two things:
    It increases your horizontal position by X units.
    It increases your depth by your aim multiplied by X.
*/
void p2(FILE *fp)
{
    int value;
    char direction[100];

    rewind(fp);

    SUBMARINE submarine;
    submarine.depth = 0;
    submarine.horizontal = 0;
    submarine.aim = 0;

    while (!feof(fp))
    {
        fscanf(fp, "%s %d", direction, &value);

        if (strcmp(direction, "forward") == 0)
        {
            submarine.horizontal += value;
            submarine.depth += (value * submarine.aim);
        }
        else if (strcmp(direction, "down") == 0)
            submarine.aim += value;
        else if (strcmp(direction, "up") == 0)
            submarine.aim -= value;
    }
    printf("p2 - %d\n", submarine.horizontal * submarine.depth);
}
int main()
{
    FILE *fp = fopen("input.txt", "r");
    p1(fp);
    p2(fp);

    return 0;
}
