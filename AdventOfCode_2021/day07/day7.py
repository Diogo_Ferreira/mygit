from math import inf

F = open("input.txt", 'r')


def read_file():
    positions = []

    line = F.readline()
    l = line.split(",")
    positions = [int(x) for x in l]

    return positions


def calculate_cost2(src, dest):
    cost = 0
    distance = abs(src - dest)

    for i in range(distance+1):
        cost += i

    return cost


def p1(positions):
    answer = 0
    finalPos = positions[len(positions)//2]

    for x in positions:
        answer += abs(x-finalPos)
    print("P1: ", answer)


def p2(positions):
    min = inf

    for i in range(1935):
        cost = 0
        for x in positions:
            cost += calculate_cost2(x, i)

        if cost < min:
            min = cost

    print("P2: ", min)


def main():
    positions = read_file()
    # positions.sort()
    # p1(positions)
    p2(positions)


main()
F.close()
