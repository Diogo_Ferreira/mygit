#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "linkedlist.h"

#define SIZE 12
static int cnt = 0;

void bin_to_dec(int bin[SIZE], int answer[2])
{
    int x;
    for (int i = 0, n = SIZE - 1; i < SIZE; i++, n--)
    {
        x = pow(2, n);
        answer[0] += (bin[i] * x);
        answer[1] += (-(bin[i] - 1) * x);
    }
}

void p1(FILE *fp)
{
    char str[SIZE + 1];
    int binary[SIZE];
    int occurrences[SIZE] = {0};
    int answer[2] = {0};

    while (!feof(fp))
    {
        fscanf(fp, "%s", str);
        for (int i = 0; i < SIZE; i++)
        {
            if (str[i] == '1')
                occurrences[i]++;
            else
                occurrences[i]--;
        }
    }

    for (int i = 0; i < SIZE; i++)
    {
        if (occurrences[i] >= 0)
            binary[i] = 1;
        else
            binary[i] = 0;
    }

    bin_to_dec(binary, answer);
    printf("p1: %d x %d = %d\n\n", answer[0], answer[1], answer[0] * answer[1]);
}

NODE *file_to_list(FILE *fp)
{
    NODE *head = NULL;
    char str[SIZE + 1];

    while (!feof(fp))
    {
        fscanf(fp, "%s", str);

        if (str[0] == '0')
            cnt--;
        else
            cnt++;

        head = add_node(head, str);
    }
    return head;
}

void p2(FILE *fp)
{
    NODE *head = NULL;
    int i = 0;

    head = file_to_list(fp);

    // Oxigen
    while (number_of_nodes() > 1)
    {

        if (cnt >= 0)
            head = free_list(head, '0', i++);
        else
            head = free_list(head, '1', i++);

        cnt = update_cnt(head, i);
    }
    printf("Oxigen %s\n\n\n", head->binary);

    rewind(fp);
    free(head);
    make0();
    cnt = 0;
    i = 0;

    head = file_to_list(fp);
    // CO2
    while (number_of_nodes() > 1)
    {
        if (cnt >= 0)
            head = free_list(head, '1', i++);
        else
            head = free_list(head, '0', i++);

        cnt = update_cnt(head, i);
    }
    printf("CO2 %s", head->binary);
}
// O2  100111101011 - 2539
// CO2 001011000101 - 709
// answer = 1 800 151

int main()
{
    FILE *fp = fopen("input.txt", "r");
    p1(fp);
    rewind(fp);
    p2(fp);

    return 0;
}
