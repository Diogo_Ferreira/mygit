#include "linkedlist.h"

static int nodes = 0;

NODE *add_node(NODE *head, char bin[13])
{
    NODE *new = malloc(sizeof(NODE));

    if (new == NULL)
    {
        printf("Error\n");
        exit(1);
    }

    strcpy(new->binary, bin);
    new->next = head;
    head = new;
    nodes++;

    return head;
}

NODE *free_list(NODE *head, char key, int index)
{
    NODE *temp = head, *prev;

    while (temp != NULL && temp->binary[index] == key)
    {
        head = temp->next;
        free(temp);
        nodes--;
        temp = head;
    }

    while (temp != NULL)
    {
        while (temp != NULL && temp->binary[index] != key)
        {
            prev = temp;
            temp = temp->next;
        }
        if (temp == NULL)
            return head;

        prev->next = temp->next;
        nodes--;
        free(temp);
        temp = prev->next;
    }
    return head;
}

int number_of_nodes()
{
    return nodes;
}

int update_cnt(NODE *head, int index)
{
    int n = 0;
    NODE *aux;

    for (aux = head; aux != NULL; aux = aux->next)
    {
        if (aux->binary[index] == '0')
            n--;
        else
            n++;
    }
    return n;
}

void print_list(NODE *head)
{
    NODE *tmp = head;

    while (tmp != NULL)
    {
        printf("%s -> ", tmp->binary);
        tmp = tmp->next;
    }
    printf("\n\n");
}

void make0()
{
    nodes = 0;
}
