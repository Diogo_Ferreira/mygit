#ifndef LINK
#define LINK

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct node
{
    char binary[13];
    struct node *next;
};
typedef struct node NODE;

NODE *add_node(NODE *head, char bin[13]);
NODE *free_list(NODE *head, char key, int index);
int number_of_nodes();
int update_cnt(NODE *head, int index);
void print_list(NODE *head);
void make0();

#endif