class MAP():
    def __init__(self):
        self.position = []
        self.mat = []
        self.visited = []

    def init(self, input):
        # Position init:
        for line in input:
            for str in line:
                self.add_position(str)

        self.visited = [False for i in range(len(self.position))]

        # Mat init:
        for i in range(len(self.position)):
            line = [int(0) for i in range(len(self.position))]
            self.mat.append(line)
        for line in input:
            i = self.get_position(line[0])
            j = self.get_position(line[1])
            self.mat[i][j] = 1
            self.mat[j][i] = 1

    def get_position(self, str):
        return self.position.index(str)

    def add_position(self, str):
        for x in self.position:
            if x == str:
                return

        self.position.append(str)

    def printAll(self):
        print(self.visited)
        print(self.position)
        for l in self.mat:
            print(l)


def read_file(Map):
    input = []

    with open("input.txt") as f:
        for line in f:
            l = line.replace('\n', '')
            l = l.split('-')
            input.append(l)
    return input


def dfs(Map, src):
    stack = []
    Map.visited = [False for i in range(len(Map.position))]
    Map.visited[(Map.get_position("start"))] = True

    if Map.position[src].islower():
        Map.visited[src] = True

    stack.append(src)
    paths = 0

    while len(stack) > 0:
        source = stack.pop()
        print(Map.position)
        print("Src: ", source)

        if source == Map.get_position("end"):
            paths += 1
            print("paths - ", paths, "\n\n")
            continue

        for next in range(len(Map.mat)):
            if Map.mat[source][next] == 1 and Map.visited[next] == False:
                stack.append(next)

                if Map.position[next].islower() and Map.position[next] != "end":
                    Map.visited[next] = True

        print("Stack: ", stack, "\n")

    return paths


def main():
    paths = 0
    Map = MAP()
    input = read_file(Map)
    Map.init(input)

    search = []
    i = Map.get_position("start")
    for j in range(len(Map.mat)):
        if Map.mat[i][j] == 1:
            search.append(j)

    for src in search:
        paths += dfs(Map, src)
        print("------------------", paths)

    print("P1: ", paths)


main()
