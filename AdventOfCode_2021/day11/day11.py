def read_map():
    map = []

    with open("input.txt") as f:
        for l in f:
            line = [int(x) for x in l if x != '\n']
            map.append(line)

    return map


def print_map(map):
    for l in map:
        print(l)
    print("\n")


def get_neighbors(map, center):
    l, c = center
    neighbors = []

    # Up:
    if (l-1) >= 0:
        neighbors.append((l-1, c))
    # Down:
    if (l+1) < len(map):
        neighbors.append((l+1, c))
    # Left:
    if (c-1) >= 0:
        neighbors.append((l, c-1))
    # Right:
    if (c+1) < len(map[0]):
        neighbors.append((l, c+1))
    # Diagonal 1:
    if (l-1) >= 0 and (c-1) >= 0:
        neighbors.append((l-1, c-1))
    # Diagonal 2:
    if (l-1) >= 0 and (c+1) < len(map[0]):
        neighbors.append((l-1, c+1))
    # Diagonal 3:
    if (l+1) < len(map) and (c-1) >= 0:
        neighbors.append((l+1, c-1))
    # Diagonal 4:
    if (l+1) < len(map) and (c+1) < len(map[0]):
        neighbors.append((l+1, c+1))

    return neighbors


def reset(hasFlashed):
    allFlashed = True

    for i in range(len(hasFlashed)):
        for j in range(len(hasFlashed[0])):
            allFlashed = (hasFlashed[i][j] and allFlashed)
            hasFlashed[i][j] = False
    return allFlashed


def update_neighbors(map, center, hasFlashed, flashes):
    neighbors = get_neighbors(map, center)

    for pos in neighbors:
        i, j = pos
        if map[i][j] < 9 and hasFlashed[i][j] == False:
            map[i][j] += 1
        elif map[i][j] == 9 and hasFlashed[i][j] == False:
            map[i][j] = 0
            hasFlashed[i][j] = True
            flashes += 1
            flashes = update_neighbors(map, (i, j), hasFlashed, flashes)

    return flashes


def p1(map, hasFlashed):
    steps = 100
    flashes = 0

    for x in range(1, steps+1):

        for i in range(len(map)):
            for j in range(len(map[0])):
                if map[i][j] < 9 and hasFlashed[i][j] == False:
                    map[i][j] += 1
                elif map[i][j] == 9 and hasFlashed[i][j] == False:
                    map[i][j] = 0
                    hasFlashed[i][j] = True
                    flashes += 1
                    flashes = update_neighbors(
                        map, (i, j), hasFlashed, flashes)

        reset(hasFlashed)
    print(flashes)


def p2(map, hasFlashed):
    steps = 0
    flashes = 0
    allFlashed = False

    while not allFlashed:
        steps += 1
        for i in range(len(map)):
            for j in range(len(map[0])):
                if map[i][j] < 9 and hasFlashed[i][j] == False:
                    map[i][j] += 1
                elif map[i][j] == 9 and hasFlashed[i][j] == False:
                    map[i][j] = 0
                    hasFlashed[i][j] = True
                    flashes = update_neighbors(
                        map, (i, j), hasFlashed, flashes)
        allFlashed = reset(hasFlashed)

    print(steps)


def main():
    map = read_map()
    hasFlashed = []

    for i in range(len(map)):
        line = []
        for j in range(len(map[0])):
            line.append(False)
        hasFlashed.append(line)

    if input("Chose mode 1/2: ") == '1':
        p1(map, hasFlashed)
    else:
        p2(map, hasFlashed)


main()
