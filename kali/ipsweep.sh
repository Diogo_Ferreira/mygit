#!/bin/bash

# invoke ./ipsweep.sh xxx.xxx.x > *file* #file is not mandatory
for ip in `seq 1 254`; do #`seq a b` <=> in range(a,b) 
		ping -c 1 $1.$ip | grep "64 bytes" | cut -d " " -f 4 | tr -d ":" & #$1 -> user input \ &-> threading 
done 
