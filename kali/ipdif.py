import os 
import sys
import time
from tqdm import tqdm

command = "./ipsweep.sh " + sys.argv[1] + " > ip1.txt"
os.system(command)
print("[1st ip scan]: Completed")
time.sleep(1)
os.system("clear")

for i in tqdm (range (100),
               desc="Wating for boot...",
               ascii=False, ncols=75):
    time.sleep(1)
    
command = "./ipsweep.sh " + sys.argv[1] + " > ip2.txt"
os.system(command)
print("[2nd ip scan]: Completed\n\n")

def read_file(file):
    ip = []
    file = open(file, "r")

    for line in file:
        ip.append(line.strip())

    file.close()

    return ip


ip1_list = read_file("ip1.txt")
ip2_list = read_file("ip2.txt")

# use set difference to find the IP addresses that only appear in one of the lists
only_in_ip1 = set(ip1_list) - set(ip2_list)
only_in_ip2 = set(ip2_list) - set(ip1_list)

print("IP addresses only in ip1.txt:")
for ip in only_in_ip1:
    print(ip.strip()) 
print("-----------------------------")
print("IP addresses only in ip2.txt:")
for ip in only_in_ip2:
    print(ip.strip()) 
