#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define SIZE 7
#define swap(x, y) \
    aux = x;       \
    x = y;         \
    y = aux;

int array[SIZE] = {0};
int priority[SIZE] = {0};

void print_array(int *p)
{
    for (int i = 0; i < SIZE; i++)
    {
        printf("%d ", p[i]);
    }
    printf("\n");
}

int running_median(int n)
{
    int max = 0;
    int aux;

    for (int i = 0; i < SIZE; i++)
    {
        if (priority[i]++ > priority[max])
        {
            max = i;
        }
    }

    for (int i = max; i > 0; i--)
    {
        swap(array[i], array[i - 1]);
        swap(priority[i], priority[i - 1]);
    }

    priority[0] = 0;
    array[0] = n;

    for (int i = 0; i < SIZE - 1; i++)
    {
        if (array[i] > array[i + 1])
        {
            swap(array[i], array[i + 1]);
            swap(priority[i], priority[i + 1]);
        }
        else
        {
            break;
        }
    }

    return array[SIZE / 2];
}

int main()
{
    int n;
    int median;

    while (1)
    {
        printf("n: ");
        scanf("%d", &n);

        printf("[Start] \n");
        printf("P: ");
        print_array(priority);
        printf("A: ");
        print_array(array);

        printf("------------------\n");
        median = running_median(n);

        printf("F: ");
        print_array(priority);
        print_array(array);

        printf("\n[MEDIAN: %d]\n", median);
        printf("================================\n\n");
    }
}