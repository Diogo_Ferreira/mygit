import pygame

HEIGHT = 600
WIDTH = HEIGHT * 2
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Map_GeneratorV1.0")

# Color setup:
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
PURPLE = (128, 0, 128)
ORANGE = (255, 165, 0)
GREY = (128, 128, 128)
CYAN = (64, 224, 208)

COLORS = [WHITE, RED, GREEN, BLUE, YELLOW, PURPLE, ORANGE, GREY, CYAN, BLACK]

DISPLAY = {
    (255, 0, 0): "Red",
    (0, 255, 0): "Green",
    (255, 255, 0): "Yellow",
    (255, 255, 255): "White",
    (0, 0, 0): "Black",
    (128, 0, 128): "Purple",
    (255, 165, 0): "Orange",
    (128, 128, 128): "Grey",
    (64, 224, 208): "Cyan",
    (0, 0, 255): "Blue",
}


class Spot:
    def __init__(self, row, col, width, total_rows):
        self.row = row
        self.col = col
        self.x = row * width  # X position on screen
        self.y = col * width  # Y position on screen
        self.color = BLACK
        self.neighbors = []
        self.width = width
        self.total_rows = total_rows

    def paint(self, color):
        self.color = color

    def draw(self, win):
        pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.width))

    def get_pos(self):
        return self.row, self.col

    def reset(self):
        self.color = WHITE

    def update_neighbors(self, grid, fillColor):
        self.neighbors = []
        # DOWN
        if self.row < self.total_rows - 1 and grid[self.row + 1][self.col].color != fillColor:
            self.neighbors.append(grid[self.row + 1][self.col])
        # UP
        if self.row > 0 and grid[self.row - 1][self.col].color != fillColor:
            self.neighbors.append(grid[self.row - 1][self.col])
        # LEFT
        if self.col > 0 and grid[self.row][self.col - 1].color != fillColor:
            self.neighbors.append(grid[self.row][self.col - 1])
        # RIGHT
        if self.col < self.total_rows * 2 - 1 and grid[self.row][self.col + 1].color != fillColor:
            self.neighbors.append(grid[self.row][self.col + 1])


def make_grid(rows, width, height):  # Creates grid array
    grid = []
    gap = width // rows

    for i in range(rows):
        grid.append([])
        for j in range(rows * 2):
            spot = Spot(i, j, gap, rows)
            grid[i].append(spot)

    return grid


def draw_grid(win, rows, width, height):
    gap = width // rows

    for i in range(rows):
        pygame.draw.line(win, GREY, (0, i * gap), (width, i * gap))
        for j in range(rows * 2):
            pygame.draw.line(win, GREY, (j * gap, 0), (j * gap, width))


def draw(win, grid, rows, width, height):
    win.fill(WHITE)

    for row in grid:
        for spot in row:
            spot.draw(win)

    draw_grid(win, rows, width, height)
    pygame.display.update()


def get_clicked_pos(pos, rows, width):
    gap = width // rows
    y, x = pos

    row = y // gap
    col = x // gap

    return row, col


def fill(grid, spot, win):
    fillColor = spot.color

    queue = [spot]

    while len(queue) > 0:
        new = queue.pop(0)
        new.paint(fillColor)
        new.update_neighbors(grid, fillColor)

        for n in new.neighbors:
            if n not in queue:
                queue.append(n)


def read_map(grid):
    F = open("map.txt", "r")
    map = []

    for line in F:
        line = line.replace("\n", "")
        l = line.split(" ")
        map.append(l)

    for i in range(len(map)):
        for j in range(len(map[0])):
            try:
                grid[j][i].color = COLORS[int(map[i][j])]
            except:
                pass

    F.close()


def write_map(grid):
    F = open("map.txt", "w")

    for i in range(len(grid[0])):
        newLine = False
        for j in range(len(grid)):
            if grid[j][i].color != (0, 0, 0):
                F.write(str(COLORS.index(grid[j][i].color)) + " ")
                newLine = True
        if newLine:
            F.write("\n")
    F.close()


def main(win):  # main loop
    ROWS = 70  # Change grid size
    grid = make_grid(ROWS, WIDTH, HEIGHT)
    color = WHITE
    run = True

    while run:
        draw(win, grid, ROWS, WIDTH, HEIGHT)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            # color select
            if event.type == pygame.KEYDOWN:
                if event.key == 114:
                    read_map(grid)
                if event.key > 47 and event.key < 59:
                    color = COLORS[event.key - 48]  # Getting number
                    print(DISPLAY[color], ":", event.key - 48)
                if event.key == 119:
                    write_map(grid)

            if pygame.mouse.get_pressed()[0]:  # left mouse
                pos = pygame.mouse.get_pos()
                row, col = get_clicked_pos(pos, ROWS, WIDTH)  # grid pos
                spot = grid[row][col]
                spot.paint(color)
            elif pygame.mouse.get_pressed()[2]:  # right mouse
                pos = pygame.mouse.get_pos()
                row, col = get_clicked_pos(pos, ROWS, WIDTH)
                spot = grid[row][col]
                spot.paint(color)
                fill(grid, spot, win)

    pygame.quit()


main(WIN)
