import ai
import ui
import pygame
import rules
import time
import random
from ui import WIDTH


def turn_handler(p1_AI, p2_AI, p1Turn, board):
    AI_DELAY = 1

    if p1Turn:
        if p1_AI == 0:
            if pygame.mouse.get_pressed()[0]:
                pos = pygame.mouse.get_pos()
                y, x = ui.get_clicked_position(pos)
            else:
                return 3, 3  # No one played
        elif p1_AI == 1:
            time.sleep(AI_DELAY)
            y, x = ai.lv1(p1Turn, board)
        elif p1_AI == 2:
            time.sleep(AI_DELAY)
            y, x = ai.lv2(p1Turn, board)
        else:
            time.sleep(AI_DELAY)
            y, x = ai.lv3(p1Turn, board)
    else:
        if p2_AI == 0:
            if pygame.mouse.get_pressed()[0]:
                pos = pygame.mouse.get_pos()
                y, x = ui.get_clicked_position(pos)
            else:
                return 3, 3  # No one played
        elif p2_AI == 1:
            time.sleep(AI_DELAY)
            y, x = ai.lv1(p1Turn, board)
        elif p2_AI == 2:
            time.sleep(AI_DELAY)
            y, x = ai.lv2(p1Turn, board)
        else:
            time.sleep(AI_DELAY)
            y, x = ai.lv3(p1Turn, board)
    return y, x


def main():
    board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    gameState = 0

    p1_AI = 0
    p2_AI = 3

    # Generating random playing order
    n = random.randint(0, 1)
    if n == 1:
        p1Turn = True
    else:
        p1Turn = False

    while gameState == 0:

        # If window gets closed
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

        y, x = turn_handler(p1_AI, p2_AI, p1Turn, board)

        if x != 3:  # Someone played
            # if unocupied place piece
            board, p1Turn = rules.play(board, y, x, p1Turn)

        ui.draw(board)
        gameState = rules.check_game_state(board)

    if gameState == 3:
        text = "X Wins!!!"
    elif gameState == -3:
        text = "O Wins!!!"
    else:
        text = "Draw!!!"

    ui.display_winner(text)

    main()


if __name__ == "__main__":
    main()
