import pygame

BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
PURPLE = (128, 0, 128)
ORANGE = (255, 165, 0)
GREY = (128, 128, 128)
CYAN = (64, 224, 208)
WHITE = (255, 255, 255)

WIDTH = 800
DISTANCE = WIDTH // 3
CENTER = DISTANCE // 2
MARGIN = 30
LINE = 8

WIN = pygame.display.set_mode((WIDTH, WIDTH))
pygame.display.set_caption("TicTacToe")

pygame.font.init()
FONT = pygame.font.SysFont("comicsans", 250)
FONT2 = pygame.font.SysFont("comicsans", 180)


def draw(board):
    # Board:
    WIN.fill(WHITE)
    pygame.draw.line(
        WIN, BLACK, (WIDTH // 3, MARGIN), (WIDTH // 3, WIDTH - MARGIN), LINE
    )
    pygame.draw.line(
        WIN, BLACK, (2 * (WIDTH // 3), MARGIN), (2 *
                                                 (WIDTH // 3), WIDTH - MARGIN), LINE
    )
    pygame.draw.line(
        WIN, BLACK, (MARGIN, WIDTH // 3), (WIDTH - MARGIN, WIDTH // 3), LINE
    )
    pygame.draw.line(
        WIN, BLACK, (MARGIN, 2 * (WIDTH // 3)), (WIDTH -
                                                 MARGIN, 2 * (WIDTH // 3)), LINE
    )

    pieces_handler(board)

    pygame.display.update()


def pieces_handler(board):
    for y in range(3):
        for x in range(3):
            if board[y][x] == 1:
                draw_cross(x, y)
            elif board[y][x] == -1:
                draw_circle(x, y)


def draw_cross(x, y):
    x *= DISTANCE
    y *= DISTANCE
    margin = MARGIN * 2
    size = DISTANCE - margin

    pygame.draw.line(WIN, RED, (margin + x, margin + y),
                     (size + x, size + y), LINE + 2)
    pygame.draw.line(WIN, RED, (margin + x, size + y),
                     (size + x, margin + y), LINE + 2)


def draw_circle(x, y):
    x *= DISTANCE
    y *= DISTANCE
    radious = CENTER - MARGIN * 2

    pygame.draw.circle(WIN, BLUE, (CENTER + x, CENTER + y), radious, LINE)


def get_clicked_position(pos):
    x, y = pos
    y //= DISTANCE
    x //= DISTANCE

    return y, x


def display_winner(text):
    text = FONT.render(text, 1, ORANGE)
    WIN.blit(
        text,
        (
            WIDTH // 2 - text.get_width() // 2,
            WIDTH // 2 - text.get_height() // 2,
        ),
    )
    pygame.display.update()
    pygame.time.delay(2000)
