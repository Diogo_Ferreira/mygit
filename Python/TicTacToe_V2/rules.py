def check_game_state(board):
    control = board[0][0] + board[1][1] + board[2][2]  # diagonal win
    if control == 3 or control == -3:
        return control

    control = board[0][2] + board[1][1] + board[2][0]  # diagonal win 2
    if control == 3 or control == -3:
        return control

    control = board[0][0] + board[1][0] + board[2][0]  # vertical win c1
    if control == 3 or control == -3:
        return control

    control = board[0][1] + board[1][1] + board[2][1]  # vertical win c2
    if control == 3 or control == -3:
        return control

    control = board[0][2] + board[1][2] + board[2][2]  # vertical win c3
    if control == 3 or control == -3:
        return control

    control = board[0][0] + board[0][1] + board[0][2]  # horizontal win l1
    if control == 3 or control == -3:
        return control

    control = board[1][0] + board[1][1] + board[1][2]  # horizontal win l2
    if control == 3 or control == -3:
        return control

    control = board[2][0] + board[2][1] + board[2][2]  # horizomntal win l3
    if control == 3 or control == -3:
        return control

    control = 1
    for i in range(3):
        for j in range(3):
            control *= board[i][j]

    if control == 0:  # not finnished
        return 0
    else:
        return 1


def play(board, y, x, p1Turn):

    if board[y][x] == 0:
        if p1Turn:
            board[y][x] = 1
            return board, not p1Turn
        else:
            board[y][x] = -1
            return board, not p1Turn

    return board, p1Turn
