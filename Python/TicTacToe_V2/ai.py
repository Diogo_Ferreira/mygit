from math import inf
import random
import rules


# ********************************************************************************************************* #
# * Auxiliar Functions                                                                                      #
# ********************************************************************************************************* #


def free_positions(board):
    freePos = []

    for i in range(3):
        for j in range(3):
            if board[i][j] == 0:
                freePos.append((i, j))

    return freePos


def board_copy(src):
    dest = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    for i in range(3):
        for j in range(3):
            dest[i][j] = src[i][j]
    return dest


# ********************************************************************************************************* #
# * LV 1 AI                                                                                                 #
# *     This AI level plays in a random free position                                                       #
# ********************************************************************************************************* #
def lv1(p1Turn, board):
    freePos = free_positions(board)

    n = random.randint(0, len(freePos) - 1)
    return freePos[n]


# ********************************************************************************************************* #
# * LV 2 AI                                                                                                 #
# *     This AI will check every available position and return the cordinates based on:                     #
# *         - A play that results in a win;                                                                 #
# *         - If this doesn't happen, a position tha will stop the opponent from winning;                   #
# *         - Else a position chosen based on the positionValues;                                           #
# ********************************************************************************************************* #
def lv2(p1Turn, board):
    poisitionValues = [
        [2, 1, 2],
        [1, 3, 1],
        [2, 1, 2],
    ]  # assigning points to each position on the board
    freePos = free_positions(board)
    score = 0
    allZeros = True  # Chose random spot in case of first play

    if p1Turn:
        piece = 1
    else:
        piece = -1

    for i in freePos:
        fake_board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

        # Initializing fake_board:
        for a in range(3):
            for b in range(3):
                fake_board[a][b] = board[a][b]
                if board[a][b] != 0:
                    allZeros = False

        if allZeros:
            fY, fX = lv1(p1Turn, board)

        y, x = i
        fake_board[y][x] = piece
        value = rules.check_game_state(fake_board)

        # Checking for winning pos or draw pos:
        if value * piece == 3:  # winning play
            fY, fX = y, x
            break

        # Checking if it can stop opponet from winning:
        fake_board[y][x] = -piece
        value = rules.check_game_state(fake_board)

        if value * piece == -3:  # make draw
            fY, fX = y, x
            score = 10

        # Chosing next play based on positionValues:
        if poisitionValues[y][x] > score:
            fY, fX = y, x
            score = poisitionValues[y][x]

    return fY, fX


# ********************************************************************************************************* #
# * LV 3 AI                                                                                                 #
# *     This AI will preview every possible game scenario and choose the best one                           #
# ********************************************************************************************************* #
def lv3(maximizingPlayer, board):

    if maximizingPlayer:
        bestScore = -inf
        piece = 1
    else:
        bestScore = inf
        piece = -1

    freePos = free_positions(board)

    for position in freePos:
        y, x = position
        test_board = board_copy(board)

        test_board[y][x] = piece
        score = minimax(not maximizingPlayer, test_board)  # ? not?

        if maximizingPlayer:
            if score > bestScore:
                bestScore = score
                bestMove = position
        else:
            if score < bestScore:
                bestScore = score
                bestMove = position
    return bestMove


def minimax(maximizingPlayer, board):
    score = rules.check_game_state(board)

    if score != 0:
        return score

    if maximizingPlayer:
        bestScore = -inf
        piece = 1
    else:
        bestScore = inf
        piece = -1

    freePos = free_positions(board)

    for position in freePos:
        y, x = position

        board[y][x] = piece
        score = minimax(not maximizingPlayer, board)  # ? not?
        board[y][x] = 0

        if maximizingPlayer:
            bestScore = max(score, bestScore)
        else:
            bestScore = min(score, bestScore)

    return bestScore
