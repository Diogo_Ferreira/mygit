import pygame
from pygame.locals import *
from pygame import mixer
import gameRules
from pieces import Block
import ui
import os

FPS = 60
SPEED = FPS // 4
H_SPEED = FPS // 10

dir = os.path.join(os.getcwd(), "Tetris_music.ogg")

mixer.init()
mixer.music.load(dir)
mixer.music.play()

# Creating blank board wid two layers of margin (-1)
def board_init(board):
    for i in range(18):
        line = []
        for j in range(12):
            line.append(0)
        board.append(line)


def main():

    board = []
    board_init(board)

    clock = pygame.time.Clock()

    gameOver = False
    downFlag = 0
    h_flag = [0, 0]
    block = Block(board)

    while not gameOver:
        clock.tick(FPS)

        activePiece = not block.is_settled(board)

        if activePiece:
            downFlag += 1
            if downFlag >= FPS:
                downFlag = 0
                block.clean_trace(board)
                block.move_down(board)
        else:
            if gameRules.gameOver(board):
                break
            gameRules.line_manager(board)
            block = Block(board)
            activePiece = True

        for event in pygame.event.get():
            # If window gets closed
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

            # Single button press
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:  # Rotating piece
                    block.clean_trace(board)
                    block.rotate()
                    block.spawn(board)
                if event.key == pygame.K_LEFT:
                    h_flag[0] = 0
                    block.move_left(board)
                if event.key == pygame.K_RIGHT:
                    h_flag[1] = 0
                    block.move_right(board)

        # Holding key
        keys_pressed = pygame.key.get_pressed()
        if keys_pressed[pygame.K_DOWN]:  # Acelerating piece
            downFlag += SPEED
        if keys_pressed[pygame.K_LEFT]:
            h_flag[0] += 1
            if h_flag[0] >= H_SPEED:
                h_flag[0] = 0
                block.move_left(board)
        if keys_pressed[pygame.K_RIGHT]:
            h_flag[1] += 1
            if h_flag[1] >= H_SPEED:
                h_flag[1] = 0
                block.move_right(board)
        ui.draw(board)
    main()


if __name__ == "__main__":
    main()
