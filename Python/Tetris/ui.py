import pygame

WIDTH = 500
HEIGHT = int(1.5 * WIDTH)
SIZE = WIDTH // 12
GRID_LINE = 1
BORDER = 10

WIN = pygame.display.set_mode((WIDTH, HEIGHT))  # display setup
pygame.display.set_caption("Tetris")

# Color setup:
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BACKGROUND = (0, 0, 90)
GRID = (16, 56, 100)
PURPLE = (150, 0, 128)
ORANGE = (255, 165, 0)
GREY = (128, 128, 128)
CYAN = (64, 224, 208)


def draw_grid():

    # Vertical Lines
    for i in range(WIDTH // SIZE):
        pygame.draw.line(WIN, GRID, (i * SIZE, 0), (i * SIZE, HEIGHT), GRID_LINE)

    # Horizontal Lines
    for i in range(HEIGHT // SIZE):
        pygame.draw.line(WIN, GRID, (0, i * SIZE), (WIDTH, i * SIZE), GRID_LINE)

    # Borders
    pygame.draw.line(WIN, BLACK, (0, 0), (WIDTH, 0), BORDER)  # H1
    pygame.draw.line(WIN, BLACK, (0, HEIGHT - BORDER // 4), (WIDTH, HEIGHT - BORDER // 4), BORDER)  # H2
    pygame.draw.line(WIN, BLACK, (0, 0), (0, HEIGHT), BORDER)  # V1
    pygame.draw.line(WIN, BLACK, (WIDTH - BORDER // 4, 0), (WIDTH - BORDER // 4, HEIGHT), BORDER)  # V2


def paint(board):
    for i in range(18):
        for j in range(12):
            if board[i][j] == 1:
                pygame.draw.rect(WIN, RED, (j * SIZE, i * SIZE, SIZE, SIZE))
            elif board[i][j] == 2:
                pygame.draw.rect(WIN, GREEN, (j * SIZE, i * SIZE, SIZE, SIZE))
            elif board[i][j] == 3:
                pygame.draw.rect(WIN, YELLOW, (j * SIZE, i * SIZE, SIZE, SIZE))
            elif board[i][j] == 4:
                pygame.draw.rect(WIN, WHITE, (j * SIZE, i * SIZE, SIZE, SIZE))
            elif board[i][j] == 5:
                pygame.draw.rect(WIN, CYAN, (j * SIZE, i * SIZE, SIZE, SIZE))
            elif board[i][j] == 6:
                pygame.draw.rect(WIN, ORANGE, (j * SIZE, i * SIZE, SIZE, SIZE))
            elif board[i][j] == 7:
                pygame.draw.rect(WIN, PURPLE, (j * SIZE, i * SIZE, SIZE, SIZE))
            else:
                pygame.draw.rect(WIN, BACKGROUND, (j * SIZE, i * SIZE, SIZE, SIZE))


def draw(board):
    WIN.fill(BACKGROUND)
    paint(board)
    draw_grid()
    pygame.display.update()
