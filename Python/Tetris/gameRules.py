# This function will check the board for a lines without 0's and clean them
def line_manager(board):
    for i in range(18):
        hasZero = False
        for j in range(12):
            if board[i][j] == 0:
                hasZero = True
                break

        if not hasZero:
            for j in range(12):
                board[i][j] = 0
            all_down(board, i)


def all_down(board, limit):
    for i in range(limit, 0, -1):
        for j in range(12):
            board[i][j] = board[i - 1][j]
            board[i - 1][j] = 0


def gameOver(board):
    for j in range(12):
        if board[0][j] != 0:
            return True
    return False
