from random import randint
import numpy

# O I S Z L J T
class Block:
    def __init__(self, board):
        self.matrix = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

        # Selecting piece to generate
        n = randint(0, 6)
        if n == 0:
            self.make_O()
        elif n == 1:
            self.make_I()
        elif n == 2:
            self.make_S()
        elif n == 3:
            self.make_Z()
        elif n == 4:
            self.make_L()
        elif n == 5:
            self.make_J()
        else:
            self.make_T()

        self.vBounds = self.vertical_bounderies()
        self.hBounds = self.horizontal_bounderies()
        self.x = 4  # Current position x
        self.y = -1  # Current position y
        self.setteld = False
        self.spawn(board)

    def make_O(self):
        self.matrix = [[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]]

    def make_I(self):
        self.matrix = [[0, 0, 0, 0], [2, 2, 2, 2], [0, 0, 0, 0], [0, 0, 0, 0]]

    def make_S(self):
        self.matrix = [[0, 0, 0, 0], [3, 3, 0, 0], [0, 3, 3, 0], [0, 0, 0, 0]]

    def make_Z(self):
        self.matrix = [[0, 0, 0, 0], [0, 4, 4, 0], [4, 4, 0, 0], [0, 0, 0, 0]]

    def make_L(self):
        self.matrix = [[0, 0, 0, 0], [0, 5, 5, 5], [0, 5, 0, 0], [0, 0, 0, 0]]

    def make_J(self):
        self.matrix = [[0, 0, 0, 0], [6, 0, 0, 0], [6, 6, 6, 0], [0, 0, 0, 0]]

    def make_T(self):
        self.matrix = [[0, 0, 0, 0], [7, 7, 7, 0], [0, 7, 0, 0], [0, 0, 0, 0]]

    def rotate(self):
        N = len(self.matrix[0])
        for i in range(N // 2):
            for j in range(i, N - i - 1):
                temp = self.matrix[i][j]
                self.matrix[i][j] = self.matrix[N - 1 - j][i]
                self.matrix[N - 1 - j][i] = self.matrix[N - 1 - i][N - 1 - j]
                self.matrix[N - 1 - i][N - 1 - j] = self.matrix[j][N - 1 - i]
                self.matrix[j][N - 1 - i] = temp

    def vertical_bounderies(self):
        vBounds = [0, 0]
        i = 0
        for l in range(4):
            n = numpy.sum(self.matrix[l])
            if n == 0:
                vBounds[i] += 1
            else:
                i = 1

        return (vBounds[0], 3 - vBounds[1])

    def horizontal_bounderies(self):
        self.rotate()
        hBounds = [0, 0]
        i = 0
        for l in range(4):
            n = numpy.sum(self.matrix[l])
            if n == 0:
                hBounds[i] += 1
            else:
                i = 1

        for i in range(3):
            self.rotate()

        return (hBounds[0], 3 - hBounds[1])

    def spawn(self, board):
        x = self.x
        y = self.y

        # Copying piece to board
        for i in range(4):
            for j in range(4):
                if self.matrix[i][j] != 0:
                    try:
                        board[y + i][x + j] = self.matrix[i][j]
                    except:
                        pass

    def move_down(self, board):
        self.vBounds = self.vertical_bounderies()
        self.hBounds = self.horizontal_bounderies()
        self.y += 1
        self.spawn(board)

    def move_left(self, board):
        if self.x + self.hBounds[0] > 0:
            self.clean_trace(board)
            self.x -= 1
            self.spawn(board)

    def move_right(self, board):
        if self.x + self.hBounds[1] < 11:
            self.clean_trace(board)
            self.x += 1
            self.spawn(board)

    def clean_trace(self, board):
        x = self.x
        y = self.y

        left, right = self.hBounds[0], self.hBounds[1]
        top, bottom = self.vBounds[0], self.vBounds[1]

        for i in range(4):
            for j in range(4):
                if self.matrix[i][j] != 0:
                    try:
                        board[y + i][x + j] = 0
                    except:
                        pass

    def is_settled(self, board):
        x = self.x
        y = self.y

        bottom = self.vBounds[1]

        if self.y + bottom == 17:
            self.setteld = True
            return self.setteld

        for i in range(4):
            for j in range(4):
                try:
                    if self.matrix[i][j] != 0:
                        if i == 3 and board[y + i + 1][x + j] != 0:  # (i == 3: last row)
                            self.setteld = True
                        elif self.matrix[i + 1][j] == 0 and board[y + i + 1][x + j] != 0:
                            self.setteld = True
                except:
                    pass
        return self.setteld
