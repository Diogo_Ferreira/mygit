import os
import pygame

pygame.font.init()
pygame.mixer.init()  # to handle sounds

FPS = 60
VEL = 10
BULLET_VEL = 20
MAX_BUL = 1000
BLACK = (0, 0, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
WIDTH, HEIGHT = 900, 500
SPACESHIP_WIDTH, SPACESHIP_HEIGHT = 55, 44
BORDER = pygame.Rect((WIDTH // 2 - 5), 0, 10, HEIGHT)
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Space Ships")  # Window name

YELLOW_HIT = pygame.USEREVENT + 1
RED_HIT = pygame.USEREVENT + 2

HEALTH_FONT = pygame.font.SysFont("comicsans", 40)
WINNER_FONT = pygame.font.SysFont("comicsans", 80)

# * Sound:
dir = os.path.join(os.getcwd(), "Assets", "Grenade+1.mp3")
BULLE_HIT_SOUND = pygame.mixer.Sound(dir)
dir = os.path.join(os.getcwd(), "Assets", "Gun+Silencer.mp3")
BULLE_FIRE_SOUND = pygame.mixer.Sound(dir)


# * Images:

dir = os.path.join(os.getcwd(), "Assets", "spaceship_yellow.png")
YELLOW_SPACESHIP = pygame.image.load(dir)
YELLOW_SPACESHIP = pygame.transform.scale(
    YELLOW_SPACESHIP, (SPACESHIP_WIDTH, SPACESHIP_HEIGHT))
YELLOW_SPACESHIP = pygame.transform.rotate(YELLOW_SPACESHIP, 90)

dir = os.path.join(os.getcwd(), "Assets", "spaceship_red.png")
RED_SPACESHIP = pygame.image.load(dir)
RED_SPACESHIP = pygame.transform.scale(
    RED_SPACESHIP, (SPACESHIP_WIDTH, SPACESHIP_HEIGHT))
RED_SPACESHIP = pygame.transform.rotate(RED_SPACESHIP, 270)

dir = os.path.join(os.getcwd(), "Assets", "space.png")
SPACE = pygame.image.load(dir)


def draw_window(red, yellow, red_bullets, yellow_bullets, red_health, yellow_health):
    # WIN.fill(WHITE)
    WIN.blit(SPACE, (0, 0))  # background
    pygame.draw.rect(WIN, BLACK, BORDER)  # drawing BORDER

    # Health display
    red_health_text = HEALTH_FONT.render(
        "Health: " + str(red_health), 1, WHITE)
    yellow_health_text = HEALTH_FONT.render(
        "Health: " + str(yellow_health), 1, WHITE)
    WIN.blit(red_health_text, (WIDTH - red_health_text.get_width() - 10, 10))
    WIN.blit(yellow_health_text, (10, 10))

    # Drawing Spaceships:
    WIN.blit(YELLOW_SPACESHIP, (yellow.x, yellow.y))
    WIN.blit(RED_SPACESHIP, (red.x, red.y))

    for bullet in red_bullets:
        pygame.draw.rect(WIN, RED, bullet)
    for bullet in yellow_bullets:
        pygame.draw.rect(WIN, YELLOW, bullet)

    pygame.display.update()


def draw_winner(text):
    draw_text = WINNER_FONT.render(text, 1, WHITE)
    WIN.blit(draw_text, (WIDTH // 2 - draw_text.get_width() //
             2, HEIGHT // 2 - draw_text.get_height() // 2))
    pygame.display.update()
    pygame.time.delay(4000)


def yellow_handle_movement(keys_pressed, yellow):
    if keys_pressed[pygame.K_a] and yellow.x - VEL > 0:
        yellow.x -= VEL
    if keys_pressed[pygame.K_d] and yellow.x + VEL + yellow.width < BORDER.x + 20:
        yellow.x += VEL
    if keys_pressed[pygame.K_w] and yellow.y - VEL > 0:
        yellow.y -= VEL
    if keys_pressed[pygame.K_s] and yellow.y + VEL + yellow.height < HEIGHT - 18:
        yellow.y += VEL


def red_handle_movement(keys_pressed, red):
    if keys_pressed[pygame.K_LEFT] and red.x - VEL > BORDER.x + BORDER.width:
        red.x -= VEL
    if keys_pressed[pygame.K_RIGHT] and red.x + VEL + red.width < WIDTH + 15:
        red.x += VEL
    if keys_pressed[pygame.K_UP] and red.y - VEL > 0:
        red.y -= VEL
    if keys_pressed[pygame.K_DOWN] and red.y + VEL + red.height < HEIGHT - 18:
        red.y += VEL


def handle_bullets(yellow_bulets, red_bulets, yellow, red):

    for bullet in yellow_bulets:
        bullet.x += BULLET_VEL
        if red.colliderect(bullet):  # if yellow and bullet collided
            pygame.event.post(pygame.event.Event(RED_HIT))
            yellow_bulets.remove(bullet)
        if bullet.x > WIDTH:
            yellow_bulets.remove(bullet)

    for bullet in red_bulets:
        bullet.x -= BULLET_VEL
        if yellow.colliderect(bullet):  # if yellow and bullet collided
            pygame.event.post(pygame.event.Event(YELLOW_HIT))
            red_bulets.remove(bullet)
        if bullet.x < 0:
            red_bulets.remove(bullet)


def main():
    # rectangle to represent yellow player
    yellow = pygame.Rect(100, 445, SPACESHIP_WIDTH, SPACESHIP_HEIGHT)
    # rectangle to represent red player
    red = pygame.Rect(800, 445, SPACESHIP_WIDTH, SPACESHIP_HEIGHT)

    yellow_bulets = []
    red_bulets = []
    yellow_health = 10
    red_health = 10

    clock = pygame.time.Clock()
    run = True

    while run:
        clock.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # If window gets closed
                run = False
                pygame.quit()

            # If fire button was pressed (this function doesn't allow key holds)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LSHIFT and len(yellow_bulets) < MAX_BUL:
                    bullet = pygame.Rect(
                        yellow.x + yellow.width - 5, yellow.y + yellow.height // 2 + 5, 10, 5)
                    yellow_bulets.append(bullet)
                    BULLE_FIRE_SOUND.play()
                if event.key == pygame.K_RSHIFT and len(red_bulets) < MAX_BUL:
                    bullet = pygame.Rect(
                        red.x + 5, red.y + red.height // 2 + 5, 10, 5)
                    red_bulets.append(bullet)
                    BULLE_FIRE_SOUND.play()

            if event.type == YELLOW_HIT:
                yellow_health -= 1
                BULLE_HIT_SOUND.play()
            if event.type == RED_HIT:
                red_health -= 1
                BULLE_HIT_SOUND.play()

        winner_text = ""
        if yellow_health <= 0:
            winner_text = "Red Wins!"
        if red_health <= 0:
            winner_text = "Yellow Wins!"
        if winner_text != "":
            draw_winner((winner_text))
            run = False

        keys_pressed = pygame.key.get_pressed()  # store pressed keys
        yellow_handle_movement(keys_pressed, yellow)
        red_handle_movement(keys_pressed, red)

        handle_bullets(yellow_bulets, red_bulets, yellow, red)

        draw_window(red, yellow, red_bulets, yellow_bulets,
                    red_health, yellow_health)

    main()


#! Only run game if this file is run
if __name__ == "__main__":
    main()
