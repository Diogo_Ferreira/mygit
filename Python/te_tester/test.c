#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char line[100];

    while (1)
    {
        FILE *fp = fopen("input.txt", "r");

        if (fgets(line, sizeof(line), fp) != NULL)
        {
            printf("%s\n", line);
        }

        fclose(fp);
    }

    return 0;
}