import pygame
import threading

WHITE = (255, 255, 255)


class Sensor:
    def __init__(self, x, y, name):
        self.name = name
        self.button_size = 58
        self.button_cords = [(14 + x, 49 + y), (93 + x, 51 + y), (16 + x, 122 + y), (93 + x, 122 + y)]
        self.adc = 0
    
    def click(self, button_id):
        value = [-0.025, 0.025, -0.1, 0.1]
        x = int(value[button_id] * 4095)

        self.add_to_adc(x)


    def add_to_adc(self, value):
        raw_adc = self.adc + value

        if raw_adc < 0:
            self.adc = 0
        elif raw_adc > 4095:
            self.adc = 4095
        else:
            self.adc = raw_adc
    
        print(self.name, self.adc, "\n---------------")



class UI:
    def __init__(self, width):
        self.width = width
        self.height = width // 1.5
        self.window = pygame.display.set_mode((self.width, self.height))

        pygame.display.set_caption("TE_Tester")

        picture = pygame.image.load("background.png")
        self.background = pygame.transform.scale(picture, (self.width, self.height))
        
        self.sensors = [Sensor(15, 11, "APPS0"), Sensor(218, 12, "APPS1"), Sensor(415, 12, "ACC"), Sensor(18, 208, "BE"), Sensor(217, 209, "BP"), Sensor(414, 205, "BRAKE")]



    def click_handler(self, pos):
        x, y = pos

        for sensor in self.sensors:
            for i in range(4):
                xb, yb = sensor.button_cords[i];

                if(x >= xb and x <= xb + sensor.button_size and y >= yb and y <= yb + sensor.button_size):
                    if sensor.name == "ACC":
                        value = [-0.025, 0.025, -0.1, 0.1]
                        x = int(value[i] * 4095)
                        self.sensors[0].add_to_adc(x)
                        self.sensors[1].add_to_adc(x)
                    elif sensor.name == "BRAKE":
                        value = [-0.025, 0.025, -0.1, 0.1]
                        x = int(value[i] * 4095)
                        self.sensors[3].add_to_adc(x)
                        self.sensors[4].add_to_adc(x)
                    else:
                        sensor.click(i)



    def update_dsplay(self):
        self.window.fill(WHITE)
        self.window.blit(self.background, (0, 0))

        pygame.display.update()

def main():
    global data
    ui = UI(600)

    run = True
    while run:
        file = open("input.txt", "w")


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

            if pygame.mouse.get_pressed()[0]:  # left mouse
                pos = pygame.mouse.get_pos()
                ui.click_handler(pos)

        # Convert the integers to strings and join them with spaces
        data = str(ui.sensors[0].adc) + " " + str(ui.sensors[1].adc) + " " + str(ui.sensors[3].adc) + " " + str(ui.sensors[4].adc) 
        ui.update_dsplay()
        
        file.write(data)
        file.close()        



if __name__ == "__main__":
    main()
