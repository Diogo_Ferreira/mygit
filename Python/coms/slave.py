import json
from time import sleep

fp = open("buffer.json", "r+")


def read():
    fp.seek(0)
    file = json.loads(fp.read())  # read
    data = json.dumps(file, indent=2)
    print(data)


while True:
    read()
    sleep(1)
