import json

dict = {
    "motors": {
        "fl": 0,
        "fr": 0,
        "rl": 0,
        "rr": 0,
    }
}

fp = open("buffer.json", "r+")


def write():
    fp.seek(0)
    json.dump(dict, fp, indent=2)
    fp.truncate()


def read():
    file = json.load(fp)  # read

    for motor in file["motors"]:
        # print(dict)
        dict["motors"][motor] = file["motors"][motor] + 5

    x = json.dumps(dict, indent=2)


read()
write()
