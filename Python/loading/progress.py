from time import sleep
import pygame
import random


RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BACKGROUND = (0, 0, 90)
GRID = (16, 56, 100)
PURPLE = (150, 0, 128)
ORANGE = (255, 165, 0)
GREY = (128, 128, 128)
CYAN = (90, 206, 235)

# * Macros:
WIDTH = 600
HEIGHT = WIDTH // 2
RECT = int(WIDTH / 15.3)
Y = int(HEIGHT / 1.175)
IX = int(WIDTH / 5.83)
FX = int(WIDTH / 1.165)
INCREMENT = (FX - IX) // 100

# * Inits:
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("QuizowareMaster")

picture = pygame.image.load("background.png")
BACKGROUND = pygame.transform.scale(picture, (WIDTH, HEIGHT))

pygame.font.init()
FONT_TEAMS = pygame.font.SysFont("comicsans", int(WIDTH / 8))
FONT_TIMER = pygame.font.SysFont("comicsans", int(WIDTH / 4.8))


class TEAMS:
    def __init__(self, min):
        self.submitted = 0
        self.total_time = min * 60
        self.total_teams = 60

    def update(self, time_left):
        if time_left == 0:
            return

        if self.submitted >= self.total_teams:
            return

        if time_left > self.total_time * 0.7:
            return
        if time_left < self.total_time * 0.3:
            mult = 30

        n = 5 * self.total_time / time_left
        rand = random.randint(1, self.total_time)

        if n > rand and self.submitted < self.total_teams:
            self.submitted += 1

    def print(self):
        if self.submitted < 10:
            aux = "0"
        else:
            aux = ""

        text = aux + str(self.submitted) + "/" + str(self.total_teams + 1)
        text = FONT_TEAMS.render(text, 1, BLACK)
        WIN.blit(
            text,
            (
                WIDTH // 40,
                HEIGHT // 40,
            ),
        )


class CLOCK:
    def __init__(self, min):
        self.minutes = min
        self.seconds = 0
        self.inc = (FX - IX) / (self.minutes * 60 + self.seconds)
        self.size = 0
        self.has_ended = False

    def tick(self):
        self.size += self.inc
        self.seconds -= 1
        if self.seconds <= 0:
            self.minutes -= 1
            self.seconds = 59

    def has_ended(self, Teams):
        if self.minutes < 0:
            WIN.fill(WHITE)
            WIN.blit(BACKGROUND, (0, 0))
            self.print()
            Teams.print()
            pygame.display.update()
            return True
        else:
            return False

    def make_bar(self):
        pygame.draw.line(WIN, CYAN, (IX, Y), (FX - int(self.size), Y), RECT)

    def print(self):
        self.make_bar()
        aux1 = ""
        aux2 = ""
        if self.minutes < 10:
            aux1 = "0"
        if self.seconds < 10:
            aux2 = "0"

        text = aux1 + str(self.minutes) + ":" + aux2 + str(self.seconds)
        text = FONT_TIMER.render(text, 1, RED)
        WIN.blit(
            text,
            (
                WIDTH // 2 - text.get_width() // 2,
                HEIGHT // 2 - text.get_height() // 2 + HEIGHT // 10,
            ),
        )


def draw(Clock, Teams):
    WIN.fill(WHITE)
    WIN.blit(BACKGROUND, (0, 0))

    if Clock.minutes * 60 + Clock.seconds <= 0:
        Clock.minutes = 0
        Clock.seconds = 0
        Clock.has_ended = True
    Clock.print()
    Teams.print()

    pygame.display.update()


def time_sellect():
    run = True

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # If window gets closed
                run = False
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key > 47 and event.key < 59:
                    time = event.key - 48  # Getting number
                    return time

        WIN.fill(WHITE)
        WIN.blit(BACKGROUND, (0, 0))
        pygame.display.update()


def main():
    run = True

    min = time_sellect()
    Clock = CLOCK(min)

    Teams = TEAMS(min)

    # * Main loop:
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # If window gets closed
                run = False
                pygame.quit()

        sleep(0.3)
        Teams.update(Clock.minutes * 60 + Clock.seconds)
        draw(Clock, Teams)
        sleep(0.3)
        Teams.update(Clock.minutes * 60 + Clock.seconds)
        draw(Clock, Teams)
        sleep(0.4)
        Teams.update(Clock.minutes * 60 + Clock.seconds)
        draw(Clock, Teams)

        if Clock.has_ended:
            draw(Clock, Teams)
            sleep(2)
            break
        Clock.tick()
    main()


main()
