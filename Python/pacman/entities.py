import pygame
from pygame.locals import *
from queue import PriorityQueue
from math import inf as INF


#! Map cords = EntityCords + 3

class Entity:
    def __init__(self, x, y, size, speed):
        self.x = x
        self.y = y
        self.size = size
        self.rect = pygame.Rect(x * size, y * size, size, size)
        self.speed = speed
        self.hit_list = []
        self.momentum = [0, 0]
        self.debug_conter = 0

    def move(self, movement, coliders):

        old_x = self.x
        old_y = self.y
        colided = False

        # * X:
        self.x += movement[0] * self.speed
        self.rect = pygame.Rect(
            self.x * self.size, self.y * self.size, self.size, self.size)
        self.check_colisions(coliders)
        for tile in self.hit_list:
            colided = True
            if movement[0] > 0:
                self.x = old_x
            elif movement[0] < 0:
                self.x = old_x

        # * Y:
        self.y += movement[1] * self.speed
        self.rect = pygame.Rect(
            self.x * self.size, self.y * self.size, self.size, self.size)
        self.check_colisions(coliders)
        for tile in self.hit_list:
            colided = True
            if movement[1] > 0:
                self.y = old_y
            elif movement[1] < 0:
                self.y = old_y

        if not colided:
            self.rect = pygame.Rect(
                self.x * self.size, self.y * self.size, self.size, self.size)
            self.momentum = movement
        else:
            self.x = old_x
            self.y = old_y
            colided = False

            # * X:
            self.x += self.momentum[0] * self.speed
            self.rect = pygame.Rect(
                self.x * self.size, self.y * self.size, self.size, self.size)
            self.check_colisions(coliders)
            for tile in self.hit_list:
                colided = True
                if movement[0] > 0:
                    self.x = old_x
                elif movement[0] < 0:
                    self.x = old_x

            # * Y:
            self.y += self.momentum[1] * self.speed
            self.rect = pygame.Rect(
                self.x * self.size, self.y * self.size, self.size, self.size)
            self.check_colisions(coliders)
            for tile in self.hit_list:
                colided = True
                if movement[1] > 0:
                    self.y = old_y
                elif movement[1] < 0:
                    self.y = old_y
        if colided:
            self.x = old_x
            self.y = old_y
        self.rect = pygame.Rect(
            self.x * self.size, self.y * self.size, self.size, self.size)

    def check_colisions(self, coliders):
        hit_list = []

        for tile in coliders:
            if self.rect.colliderect(tile):
                hit_list.append(tile)
        self.hit_list = hit_list

    #!debug
    def draw(self, screen):
        pygame.draw.rect(screen, (255, 255, 255), self.rect)


class Pacman(Entity):
    def __init__(self, x, y, size, speed):
        super().__init__(x, y, size, speed)
        image1 = pygame.image.load("assets/open.png").convert()
        image2 = pygame.image.load("assets/closed.png").convert()
        image1 = pygame.transform.scale(image1, (self.size + 4, self.size + 4))
        image2 = pygame.transform.scale(image2, (self.size + 4, self.size + 4))
        image1.set_colorkey((255, 255, 255))
        image2.set_colorkey((255, 255, 255))

        self.normal_animation = [image1, image2]
        self.counter = 0
        self.canTeleport = True

    def animate(self, screen):
        self.counter += 1

        if self.counter <= 7:
            index = 0
        elif self.counter <= 14:
            index = 1
        else:
            index = 0
            self.counter = 0

        image = self.normal_animation[index]

        # Right
        if self.momentum[0] > 0:
            image = pygame.transform.flip(image, True, False)

        # Up/Down
        if self.momentum[1] > 0:
            image = pygame.transform.flip(image, False, True)
            image = pygame.transform.rotate(image, 90)
        if self.momentum[1] < 0:
            image = pygame.transform.rotate(image, -90)

        screen.blit(image, (self.x * self.size, self.y * self.size))

    def interact(self, map):
        x = round(self.x) + 3
        y = round(self.y) + 3

        value = map[y][x]

        if value == 1 and self.canTeleport:
            self.x = 0
            self.y = 11
            self.canTeleport = False
        elif value == -1 and self.canTeleport:
            self.x = 26
            self.y = 11
            self.canTeleport = False
        elif value == 4 or value == 2:
            map[y][x] = 0

        if abs(value) != 1:
            self.canTeleport = True


class Ghost(Entity):
    def __init__(self, x, y, size, speed, image):
        super().__init__(x, y, size, speed)
        self.image = pygame.image.load(image).convert()
        self.image = pygame.transform.scale(
            self.image, (self.size + 4, self.size + 4))
        self.lastMove = [0, 0]
        self.momentum = [1, 0]

    def draw(self, screen):
        screen.blit(self.image, (self.x * self.size, self.y * self.size))
        # pygame.draw.rect(screen, (255, 255, 255), self.rect)

    def calculate_distance(self, p1, p2):
        p1x, p1y = p1
        p2x, p2y = p2
        return abs(p1x - p2x) + abs(p1y - p2y)

    def get_neighbors(self, p, map):
        neighbors = []
        if map[self.y - 1][self.x] != 3:
            neighbors.append((self.y-1, self.x))
        if map[self.y + 1][self.x] != 3:
            neighbors.append((self.y+1, self.x))
        if map[self.y][self.x - 1] != 3:
            neighbors.append((self.y, self.x - 1))
        if map[self.y][self.x + 1] != 3:
            neighbors.append((self.y, self.x + 1))

        return neighbors
    

    def dfs(self, dest, map):
        queue = [(int(self.y), int(self.x))]
        visited = {}
        parent = {}

        for i in range(len(map)):
            for j in range(len(map[i])):
                visited[(i,j)] = False

        while len(queue) > 0:
            new = queue.pop(0)
            visited[new] = True
            
            if new == dest:
                break

            neighbors = self.get_neighbors(new, map)

            #Empty queue
            if neighbors == None:
                break

            for n in neighbors:
                if not visited[n]:
                    queue.append(n)
                    parent[n] = new 

        aux = dest
        while parent[aux] != (self.y,self.x):
            aux = parent[aux]

        return aux
    
    def chase(self, dest, map, coliders):
        target = self.dfs(dest, map)
        move = (int(abs(self.y-target[0])), int(abs(self.x - target[1])))

        # print("[PACMAN]: ", dest)
        # print("[Ghost]: ", self.y, self.x)
        # print("Move: ", move[1], move[0])
        # print("-------------------------")

        self.move(move, coliders)

        





        



                



        