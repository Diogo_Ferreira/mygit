import pygame
from pygame.locals import *
import sys

import entities

# 27,24
WINDOW_SIZE = (945, 840)
GAP = WINDOW_SIZE[0] // 27

clock = pygame.time.Clock()
pygame.init()
pygame.display.set_caption("Pacman")
screen = pygame.display.set_mode(WINDOW_SIZE, 0, 32)

# Color setup:
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRID = (16, 56, 100)
PURPLE = (150, 0, 128)
ORANGE = (255, 165, 0)
GREY = (128, 128, 128)
CYAN = (64, 224, 208)

SPEED = 0.2


def load_map():
    F = open("map.txt")
    map = []

    for line in F:
        line = line.replace("\n", " ")
        line = line.split(" ")
        l = [int(x) for x in line if x != ""]
        map.append(l)

    F.close()
    return map


# *Draw map into surface and Create coliders
def process_map(map):
    """
    1 - Portal
    2 - O
    3 - Wall
    4 - o
    """

    screen.fill(BLACK)  # clear screen by filling it with blue

    coliders = []  # Coliders
    for i in range(len(map)):
        for j in range(len(map[0])):
            if map[i][j] == 2:
                pygame.draw.circle(
                    screen, YELLOW, ((j - 3) * GAP + GAP // 2, (i - 3) * GAP + GAP // 2), GAP // 4)
            if map[i][j] == 3:
                x = (j - 3) * GAP + int(GAP * 0.1)
                y = (i - 3) * GAP + int(GAP * 0.1)
                size = int(GAP * 0.6)

                coliders.append(pygame.Rect(x, y, GAP * 0.8, GAP * 0.8))
                pygame.draw.rect(screen, BLUE, (x + 4, y + 4, size, size))
            if map[i][j] == 4:
                pygame.draw.circle(
                    screen, YELLOW, ((j - 3) * GAP + GAP // 2, (i - 3) * GAP + GAP // 2), GAP // 8)

    return coliders


def main():
    run = True
    map = load_map()
    coliders = []  # Coliders list
    movement = [0, 0]

    # *Entities initialization:
    Pacman = entities.Pacman(12, 16, GAP, SPEED)
    RedGhost = entities.Ghost(1, 1, GAP, SPEED*0.75, "assets/red.png")

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN:
                if event.key == 119:  # W
                    movement = [0, -1]
                elif event.key == 97:  # A
                    movement = [-1, 0]
                elif event.key == 115:  # S
                    movement = [0, 1]
                elif event.key == 100:  # D
                    movement = [1, 0]

        coliders = process_map(map)

        # *Ghost Actions
        RedGhost.draw(screen)
        RedGhost.chase((Pacman.y, Pacman.x), coliders, map)

        # *Pacman Actions:
        Pacman.animate(screen)
        Pacman.move(movement, coliders)
        Pacman.interact(map)
        pygame.display.update()
        clock.tick(30)


if __name__ == "__main__":
    main()
