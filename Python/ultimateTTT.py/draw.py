# import only system from os
from os import system, name


def clear():

    # for windows
    if name == 'nt':
        _ = system('cls')

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


def charCnv(token):

    if token == 1 or token == 3:
        return 'X'
    elif token == -1 or token == -3:
        return'O'
    else:
        return ' '


def big_board(smallBoards, bigBoard):
    clear()

    little(bigBoard)

    print("                       ||                   ||                                ")
    print("      ", charCnv(smallBoards[0][0][0]), " | ", charCnv(smallBoards[0][0][1]), " | ", charCnv(smallBoards[0][0][2]), "  ||  ", charCnv(smallBoards[1][0][0]), " | ", charCnv(
        smallBoards[1][0][1]), " | ", charCnv(smallBoards[1][0][2]),  "  ||  ", charCnv(smallBoards[2][0][0]), " | ", charCnv(smallBoards[2][0][1]), " | ", charCnv(smallBoards[2][0][2]))
    print("A     ", charCnv(smallBoards[0][1][0]), " | ", charCnv(smallBoards[0][1][1]), " | ", charCnv(smallBoards[0][1][2]), "  ||  ", charCnv(smallBoards[1][1][0]), " | ", charCnv(
        smallBoards[1][1][1]), " | ", charCnv(smallBoards[1][1][2]),  "  ||  ", charCnv(smallBoards[2][1][0]), " | ", charCnv(smallBoards[2][1][1]), " | ", charCnv(smallBoards[2][1][2]))
    print("      ", charCnv(smallBoards[0][2][0]), " | ", charCnv(smallBoards[0][2][1]), " | ", charCnv(smallBoards[0][2][2]), "  ||  ", charCnv(smallBoards[1][2][0]), " | ", charCnv(
        smallBoards[1][2][1]), " | ", charCnv(smallBoards[1][2][2]),  "  ||  ", charCnv(smallBoards[2][2][0]), " | ", charCnv(smallBoards[2][2][1]), " | ", charCnv(smallBoards[2][2][2]))
    print("                       ||                   ||                                ")
    print("    =============================================================")
    print("                       ||                   ||                                ")
    print("      ", charCnv(smallBoards[3][0][0]), " | ", charCnv(smallBoards[3][0][1]), " | ", charCnv(smallBoards[3][0][2]), "  ||  ", charCnv(smallBoards[4][0][0]), " | ", charCnv(
        smallBoards[4][0][1]), " | ", charCnv(smallBoards[4][0][2]),  "  ||  ", charCnv(smallBoards[5][0][0]), " | ", charCnv(smallBoards[5][0][1]), " | ", charCnv(smallBoards[5][0][2]))
    print("B     ", charCnv(smallBoards[3][1][0]), " | ", charCnv(smallBoards[3][1][1]), " | ", charCnv(smallBoards[3][1][2]), "  ||  ", charCnv(smallBoards[4][1][0]), " | ", charCnv(
        smallBoards[4][1][1]), " | ", charCnv(smallBoards[4][1][2]),  "  ||  ", charCnv(smallBoards[5][1][0]), " | ", charCnv(smallBoards[5][1][1]), " | ", charCnv(smallBoards[5][1][2]))
    print("      ", charCnv(smallBoards[3][2][0]), " | ", charCnv(smallBoards[3][2][1]), " | ", charCnv(smallBoards[3][2][2]), "  ||  ", charCnv(smallBoards[4][2][0]), " | ", charCnv(
        smallBoards[4][2][1]), " | ", charCnv(smallBoards[4][2][2]),  "  ||  ", charCnv(smallBoards[5][2][0]), " | ", charCnv(smallBoards[5][2][1]), " | ", charCnv(smallBoards[5][2][2]))
    print("                       ||                   ||                                ")
    print("    =============================================================")
    print("                       ||                   ||                                ")
    print("      ", charCnv(smallBoards[6][0][0]), " | ", charCnv(smallBoards[6][0][1]), " | ", charCnv(smallBoards[6][0][2]), "  ||  ", charCnv(smallBoards[7][0][0]), " | ", charCnv(
        smallBoards[7][0][1]), " | ", charCnv(smallBoards[7][0][2]),  "  ||  ", charCnv(smallBoards[8][0][0]), " | ", charCnv(smallBoards[8][0][1]), " | ", charCnv(smallBoards[8][0][2]))
    print("C     ", charCnv(smallBoards[6][1][0]), " | ", charCnv(smallBoards[6][1][1]), " | ", charCnv(smallBoards[6][1][2]), "  ||  ", charCnv(smallBoards[7][1][0]), " | ", charCnv(
        smallBoards[7][1][1]), " | ", charCnv(smallBoards[7][1][2]),  "  ||  ", charCnv(smallBoards[8][1][0]), " | ", charCnv(smallBoards[8][1][1]), " | ", charCnv(smallBoards[8][1][2]))
    print("      ", charCnv(smallBoards[6][2][0]), " | ", charCnv(smallBoards[6][2][1]), " | ", charCnv(smallBoards[6][2][2]), "  ||  ", charCnv(smallBoards[7][2][0]), " | ", charCnv(
        smallBoards[7][2][1]), " | ", charCnv(smallBoards[7][2][2]),  "  ||  ", charCnv(smallBoards[8][2][0]), " | ", charCnv(smallBoards[8][2][1]), " | ", charCnv(smallBoards[8][2][2]))
    print("                       ||                   ||                                ")
    print("             1                    2                    3 \n\n")


def little(bigBoard):

    print("A    ", charCnv(bigBoard[0][0]), " | ", charCnv(
        bigBoard[0][1]), " | ", charCnv(bigBoard[0][2]))
    print("     ---------------")
    print("B    ", charCnv(bigBoard[1][0]), " | ", charCnv(
        bigBoard[1][1]), " | ", charCnv(bigBoard[1][2]))
    print("     ---------------")
    print("C    ", charCnv(bigBoard[2][0]), " | ", charCnv(
        bigBoard[2][1]), " | ", charCnv(bigBoard[2][2]))
    print("      1     2     3 \n")
