import draw
import rules
import userInput

bigBoard = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

smallBoards = [
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
]

gameState = 0
placemet = 10
forced = False

while gameState == 0:

    #! X plays: ##############################################################
    draw.big_board(smallBoards, bigBoard)

    forced = rules.forced(placemet, smallBoards)
    if forced:
        selectSmall = placemet
    else:
        selectSmall = userInput.inputBig(bigBoard, smallBoards)

    placemet = userInput.inputSmall(smallBoards[selectSmall])  # where to go
    smallBoards[selectSmall] = userInput.placePiece(placemet, smallBoards[selectSmall], 1)

    # checking boards
    bigBoard = rules.checkAll(smallBoards, bigBoard)  # update bigBoard
    gameState = rules.checkMatrix(bigBoard)

    if gameState != 0:
        break

    #! O plays: ##############################################################
    draw.big_board(smallBoards, bigBoard)

    forced = rules.forced(placemet, smallBoards)
    if forced:
        selectSmall = placemet
    else:
        selectSmall = userInput.inputBig(bigBoard, smallBoards)

    placemet = userInput.inputSmall(smallBoards[selectSmall])  # where to go
    smallBoards[selectSmall] = userInput.placePiece(placemet, smallBoards[selectSmall], -1)

    # checking boards
    bigBoard = rules.checkAll(smallBoards, bigBoard)  # update bigBoard
    gameState = rules.checkMatrix(bigBoard)


draw.big_board(smallBoards, bigBoard)

if gameState == 1:
    print("X won !")
elif gameState == -1:
    print("O won !")
else:
    print("Draw !")
