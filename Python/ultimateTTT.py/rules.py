# -1 => O wins
#  1 => X wins
#  10 => draw
#  0 => not finnished

from typing import Deque


def checkMatrix(matrix):
    control = matrix[0][0] + matrix[1][1] + \
        matrix[2][2]  # diagonal win
    if (control == 3 or control == -3):
        return (control / 3)

    control = matrix[0][2] + matrix[1][1] + \
        matrix[2][0]  # diagonal win 2
    if (control == 3 or control == -3):
        return (control / 3)

    control = matrix[0][0] + matrix[1][0] + \
        matrix[2][0]  # vertical win c1
    if (control == 3 or control == -3):
        return (control / 3)

    control = matrix[0][1] + matrix[1][1] + \
        matrix[2][1]  # vertical win c2
    if (control == 3 or control == -3):
        return (control / 3)

    control = matrix[0][2] + matrix[1][2] + \
        matrix[2][2]  # vertical win c3
    if (control == 3 or control == -3):
        return (control / 3)

    control = matrix[0][0] + matrix[0][1] + \
        matrix[0][2]  # horizontal win l1
    if (control == 3 or control == -3):
        return (control / 3)

    control = matrix[1][0] + matrix[1][1] + \
        matrix[1][2]  # horizontal win l2
    if (control == 3 or control == -3):
        return (control / 3)

    control = matrix[2][0] + matrix[2][1] + \
        matrix[2][2]  # horizomntal win l3
    if (control == 3 or control == -3):
        return (control / 3)

    control = 1
    for i in range(3):
        for j in range(3):
            control *= matrix[i][j]

    if control == 0:  # not finnished
        return 0
    else:
        return 10


def checkAll(smallBoards, bigBoard):  # Updates bigBoard
    for y in range(3):
        for x in range(3):
            bigBoard[y][x] = checkMatrix(smallBoards[3 * y + x])

    return bigBoard


def forced(placement, smallBoards):
    letters = ['A', 'B', 'C']

    if placement == 10:  # First play
        return False

    Forced = can_I_play_here(smallBoards[placement])

    if Forced:
        y = int(placement / 3)
        x = placement - (3 * y)
        print("You were forced to play in ", letters[y], end='')
        print(x+1, "!!!")
        return True
    else:
        return False


def can_I_play_here(matrix):
    flag = checkMatrix(matrix)

    if flag == 0:
        return True
    else:
        return False
