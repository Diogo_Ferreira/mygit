import rules


def inputBig(bigBoard, smallBoards):  # Choose big Board; ret: 0 - 9
    str = input("In wich board you want to play: ")

    try:
        y = ord(str[0]) - ord("A")
        x = int(str[1]) - 1
    except:
        print("Invalid Values...")
        return inputBig(bigBoard, smallBoards)

    # invalid input check
    try:
        while bigBoard[y][x] != 0:
            str = input("You cant play in this board, try again: ")
            y = ord(str[0]) - ord("A")
            x = int(str[1]) - 1
    except:
        print("Invalid Values...")
        return inputBig(bigBoard, smallBoards)

    return 3 * y + x


def inputSmall(board):  # Check for valid input; ret: 0 - 9
    str = input("\nWhere do you wanna play (inside board): ")

    try:
        y = ord(str[0]) - ord("A")
        x = int(str[1]) - 1
    except:
        print("Invalid Values...")
        return inputSmall(board)

    try:
        while board[y][x] != 0:
            str = input("Invalid position, try again: ")
            y = ord(str[0]) - ord("A")
            x = int(str[1]) - 1
    except:
        print("Invalid Values...")
        return inputSmall(board)

    return 3 * y + x


def placePiece(position, board, piece):
    y = int(position / 3)
    x = position - (3 * y)

    board[y][x] = piece

    return board
