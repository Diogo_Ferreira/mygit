map =[
[0,0,0,1,0,0,0,0,0,0],
[0,0,0,1,0,0,0,0,0,0],
[0,0,0,1,0,0,0,0,1,0],
[0,0,1,1,0,0,1,1,1,0],
[0,1,1,0,0,1,1,0,0,0],
[0,0,0,0,1,1,0,0,0,0],
[0,0,0,0,1,0,0,0,0,0],
[0,0,0,0,1,0,0,0,0,0],
[0,0,0,0,1,0,0,0,0,1],
[0,0,0,0,1,0,0,0,0,0]
]


def get_neighbors(pos, map):
    neighbors = []
    y = 0
    x = 1

    if y - 1 >= 0: 
        if map[pos[y]-1][pos[x]] == 0:
            neighbors.append((pos[y]-1, pos[x])) 
    if y + 1 <= 9:
        if map[pos[y]+1][pos[x]] == 0:
            neighbors.append((pos[y]+1, pos[x]))  
    if x - 1 >= 0: 
        if map[pos[y]][pos[x]-1] == 0:
            neighbors.append((pos[y], pos[x]-1))  
    if x + 1 <= 9: 
        if map[pos[y]][pos[x]+1] == 0:
            neighbors.append((pos[y], pos[x]+1)) 
    
    return neighbors


def dfs(dest, map):
    queue = [(0, 0)]
    visited = {}
    parent = {}

    for i in range(len(map)):
        for j in range(len(map[i])):
            visited[(i,j)] = False

    while len(queue) > 0:
        new = queue.pop(0)
        visited[new] = True
            
        if new == dest:
            break

        neighbors = get_neighbors(new, map)

        #Empty queue
        if neighbors == None:
            break

        for n in neighbors:
            if not visited[n]:
                queue.append(n)
                parent[n] = new 

    aux = dest
    while parent[aux] != (0, 0):
        print(aux)
        aux = parent[aux]

dfs((9, 9), map)

