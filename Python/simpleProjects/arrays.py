# import only system from os
from os import system, name


def clear():

    # for windows
    if name == "nt":
        _ = system("cls")

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system("clear")


def novos_valores(valores):
    clear()
    escolha = int(
        input("1 - Adicionar novos elementos;\n2 - Escrever lista do Início\n\n")
    )

    if escolha == 1:
        size = int(input("Quantos elementos queres adicionar: "))
        for i in range(0, size):
            n = int(input("Insere um valor: "))
            valores.append(n)
    else:
        size = int(input("Insere o tamanho da array: "))
        for i in range(0, size):
            n = int(input("Insere um valor: "))
            valores.append(n)

    print("\nArray: ", valores)
    return valores


def par_impar(valores, size):
    clear()
    flag = int(input("1 - impar, 2 - par: "))
    newArray = []

    for i in range(0, size):
        if (valores[i] % 2) == 0 and flag == 2:
            newArray.append(valores[i])
        elif (valores[i] % 2) != 0 and flag == 1:
            newArray.append(valores[i])
    print("Array: ", newArray)
    return newArray


def sort(valores, size):
    clear()
    flag = 1

    while flag != 0:
        flag = 0

        for i in range(0, size - 1):
            if valores[i] > valores[i + 1]:
                aux = valores[i]
                valores[i] = valores[i + 1]
                valores[i + 1] = aux
                flag += 1
        print("Sort: ", valores)

    return valores


def printArray(valores, size):
    clear()
    for i in range(size):
        print("Array[", i, "] = ", valores[i])


valores = []

size = int(input("Insere o tamanho da array: "))

for i in range(0, size):
    n = int(input("Insere um valor: "))
    valores.append(n)
print("Array: ", valores)

while 1:
    print(
        "\n\n\n1 - Inserir/Adicionar novos valores;\n2 - Imprimir Array;\n3 - Oraganizar;\n4 - Par/Impar;\n5 - Sair;"
    )
    escolha = int(input("\nO que queres fazer: "))

    if escolha == 1:
        valores = novos_valores(valores)
    elif escolha == 2:
        printArray(valores, size)
    elif escolha == 3:
        valores = sort(valores, size)
    elif escolha == 4:
        valores = par_impar(valores, size)
    else:
        break
