from argparse import RawDescriptionHelpFormatter
from ctypes.wintypes import PINT
from multiprocessing import parent_process
import queue


map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]


def print_map():
    print("\n  0 1 2 3 4 5 6")
    chars = [" ", "#", "."]
    i = 0

    for line in map:
        print(i, end=" ")
        i += 1
        for n in line:
            print(chars[n], end=" ")
        print()
    print("\n")


def init_parents():
    parents = []
    for i in range(len(map)):
        line = []
        for j in range(len(map[1])):
            line.append(None)
        parents.append(line)

    return parents


def get_neighbors(i, j):
    neighbors = []

    if map[i - 1][j] == 0:
        neighbors.append((i - 1, j))
    if map[i + 1][j] == 0:
        neighbors.append((i + 1, j))
    if map[i][j - 1] == 0:
        neighbors.append((i, j - 1))
    if map[i][j + 1] == 0:
        neighbors.append((i, j + 1))

    return neighbors


def dfs():
    queue = [(1, 1)]
    parents = init_parents()
    prev = (1, 1)
    parents[1][1] = (1, 1)

    while len(queue) > 0:
        i, j = queue.pop(0)
        prev = (i, j)

        if i == len(map) - 2 and j == len(map[1]) - 2:
            break

        neighbors = get_neighbors(i, j)
        for n in neighbors:
            y, x = n
            if parents[y][x] == None:
                queue.append(n)
                parents[y][x] = i, j

    return parents


def get_path():
    path = []
    point = (len(map) - 2, len(map[1]) - 2)

    parents = dfs()

    while point != (1, 1):
        i, j = point
        path.append(point)
        point = parents[i][j]

    for point in path:
        i, j = point
        map[i][j] = 2

    map[1][1] = 2


print("\n")
print_map()
get_path()
print_map()
