naoPrimo = False
n = int(input("Insere um número: "))

if n > 1:
    for i in range(2, n):
        if (n % i) == 0:
            naoPrimo = True
            break

if naoPrimo:
    print("O número não é primo")
else:
    print("O numero é primo")
