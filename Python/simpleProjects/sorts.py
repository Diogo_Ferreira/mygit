def copy(array, size, desteny):
    for i in range(size):
        desteny.append(array[i])


def swap(array, s1, s2):
    aux = array[s1]
    array[s1] = array[s2]
    array[s2] = aux
    return array


def visualise(array, size):
    print("\n")
    for i in range(size):
        for j in range(array[i]):
            print("*", end="")
        print(" (", array[i], ")")


def bubbleSort(array, size):
    swaped = True
    bubble = []
    checks = 0
    switches = 0

    copy(array, size, bubble)

    print("Bubble Sort (", bubble, "): \n")
    visualise(bubble, size)

    while swaped:
        swaped = False

        for i in range(size - 1):
            checks += 1
            if bubble[i] > bubble[i + 1]:
                switches += 1
                bubble = swap(bubble, i, i + 1)
                swaped = True
                visualise(bubble, size)
    print("\nComparations: ", checks, "\nSwitches: ", switches)

    result = [checks, switches]
    return result


def selectionSort(array, size):
    control = int(size - 1)
    swaped = True
    max = 1
    checks = 0
    switches = 0

    selection = []
    copy(array, size, selection)

    print("SelectionSort (", selection, "): \n")
    visualise(selection, size)

    while swaped:
        max = selection[control]
        swaped = False

        for i in range(0, control):
            checks += 1
            if selection[i] > max:
                max = selection[i]
                index = i
                swaped = True

        if swaped:
            switches += 1
            swap(selection, index, control)
            control -= 1
        else:
            break
        visualise(selection, size)
    print("\nComparations: ", checks, "\nSwitches: ", switches)

    result = [checks, switches]
    return result


def cocktailSort(array, size):
    right = int(size - 1)
    left = 0
    swapedL = True
    swapedR = True
    cocktail = []
    checks = 0
    switches = 0

    copy(array, size, cocktail)

    print("Cocktail Sort (", cocktail, "): \n")
    visualise(cocktail, size)

    while swapedL or swapedR:
        swapedR = False
        swapedL = False
        max = cocktail[right]
        min = cocktail[left]

        for i in range(left, right):
            checks += 1
            if cocktail[i] > max:
                max = cocktail[i]
                max_index = i
                swapedR = True
            elif cocktail[i] < min:
                min = cocktail[i]
                min_index = i
                swapedL = True

        if swapedR:
            switches += 1
            swap(cocktail, max_index, right)
        if swapedL:
            switches += 1
            swap(cocktail, min_index, left)
        left += 1
        right -= 1

        visualise(cocktail, size)

    print("\nComparations: ", checks, "\nSwitches: ", switches)

    result = [checks, switches]
    return result


array = []
size = int(input("How many numbers do you want: "))

for i in range(size):
    n = int(input("Insert a number: "))
    array.append(n)

print("\nArray: ", array, "\n\n")

resultBubble = bubbleSort(array, size)
print("\n\n\n")

resultSelection = selectionSort(array, size)
print("\n\n\n")

resultCoktail = cocktailSort(array, size)
print("\n\n\n")

print("\n\nRESULTS:")
print("    Bubble Sort: ")
