# import only system from os
from os import system, name


def clear():

    # for windows
    if name == "nt":
        _ = system("cls")

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system("clear")


tabuleiro = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]


def desenhar_tabuleiro():
    clear()
    pecas = [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]

    for l in range(0, 3):
        for c in range(0, 3):
            if tabuleiro[l][c] == 1:
                pecas[l][c] = "X"
            elif tabuleiro[l][c] == -1:
                pecas[l][c] = "O"
            else:
                pecas[l][c] = " "

    print("A   ", pecas[0][0], " | ", pecas[0][1], " | ", pecas[0][2])
    print("    ---------------")
    print("B   ", pecas[1][0], " | ", pecas[1][1], " | ", pecas[1][2])
    print("    ---------------")
    print("C   ", pecas[2][0], " | ", pecas[2][1], " | ", pecas[2][2])
    print("     1     2     3 \n\n")


def inputValidation(str, player):
    y = ord(str[0]) - ord("A")
    x = int(str[1]) - 1

    if tabuleiro[y][x] == 0:
        tabuleiro[y][x] = player
    else:
        str = input("Coordenadas inválidas, tente outra vez: ")
        inputValidation(str, player)


def gameManager():
    control = tabuleiro[0][0] + tabuleiro[1][1] + tabuleiro[2][2]  # diagonal win
    if control == 3 or control == -3:
        return control

    control = tabuleiro[0][2] + tabuleiro[1][1] + tabuleiro[2][0]  # diagonal win 2
    if control == 3 or control == -3:
        return control

    control = tabuleiro[0][0] + tabuleiro[1][0] + tabuleiro[2][0]  # vertical win c1
    if control == 3 or control == -3:
        return control

    control = tabuleiro[0][1] + tabuleiro[1][1] + tabuleiro[2][1]  # vertical win c2
    if control == 3 or control == -3:
        return control

    control = tabuleiro[0][2] + tabuleiro[1][2] + tabuleiro[2][2]  # vertical win c3
    if control == 3 or control == -3:
        return control

    control = tabuleiro[0][0] + tabuleiro[0][1] + tabuleiro[0][2]  # horizontal win l1
    if control == 3 or control == -3:
        return control

    control = tabuleiro[1][0] + tabuleiro[1][1] + tabuleiro[1][2]  # horizontal win l2
    if control == 3 or control == -3:
        return control

    control = tabuleiro[2][0] + tabuleiro[2][1] + tabuleiro[2][2]  # horizomntal win l3
    if control == 3 or control == -3:
        return control

    control = 1
    for i in range(3):
        for j in range(3):
            control *= tabuleiro[i][j]

    if control == 0:  # not finnished
        return 0
    else:
        return 1


gameState = 0
while gameState == 0:
    desenhar_tabuleiro()
    str = input("Jogador 1, insira as coordenadas desejadas (ex: A2): ")
    inputValidation(str, 1)
    desenhar_tabuleiro()

    if gameManager() != 0:
        gameState = gameManager()
        break

    str = input("Jogador 2, insira as coordenadas desejadas (ex: A2): ")
    inputValidation(str, -1)

    gameState = gameManager()

if gameState == 3:
    print("O jogador 1 ganhou!")
elif gameState == -3:
    print("O jogador 2 ganhou!")
else:
    print("Empate!")
