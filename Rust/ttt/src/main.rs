#![allow(unused_variables, unused_mut, dead_code)]

use std::default::Default;
use std::io::{self, Write};

macro_rules! clear_screen {
    () => {
        println!(".");
        // std::process::Command::new("clear").status().unwrap();
    };
}

macro_rules! input_handler {
    ($pos: ident, $board: ident, $player: ident, $get_input: ident) => {
        $pos = $get_input($player);
        while $board[$pos.0 as usize][$pos.1 as usize] != 0 {
            $pos = $get_input($player);
        }
    };
}

#[derive(PartialEq, Eq, Debug)]
enum Winner {
    X,
    O,
    DRAW,
    NONE,
}

fn print_board(board: [[i32; 3]; 3]) {
    let print_board = board
        .iter()
        .map(|row| {
            row.iter()
                .map(|&x| match x {
                    1 => 'x',
                    -1 => 'o',
                    0 => ' ',
                    _ => panic!("Unexpected value"),
                })
                .collect::<Vec<char>>()
        })
        .collect::<Vec<Vec<char>>>();

    clear_screen!();

    println!(
        "a   {} | {} | {}",
        print_board[0][0], print_board[0][1], print_board[0][2]
    );
    println!("   -----------");

    println!(
        "b   {} | {} | {}",
        print_board[1][0], print_board[1][1], print_board[1][2]
    );
    println!("   -----------");

    println!(
        "c   {} | {} | {}",
        print_board[2][0], print_board[2][1], print_board[2][2]
    );

    println!("\n    1   2   3");
}

fn treat_input(input: &String) -> Option<(i32, i32)> {
    // Invalid size:
    if input.len() != 2 {
        return None;
    }

    let parts: Vec<&str> = input.split("").filter(|s| *s != "").collect();

    // Line:
    let line = match parts[0] {
        "A" | "a" => 0,
        "B" | "b" => 1,
        "C" | "c" => 2,
        _ => {
            return None;
        }
    };

    // Column:
    let column = match parts[1].parse::<i32>() {
        Ok(n) => n - 1,
        Err(_) => {
            return None;
        }
    };

    if line < 3 && line >= 0 && column < 3 && column >= 0 {
        return Some((line, column));
    } else {
        return None;
    }
}

fn get_input(player: i32) -> (i32, i32) {
    let mut line: i32;
    let mut column: i32;
    let mut input = String::new();
    let c = if player == 1 { 'x' } else { 'o' };

    // Get input until it is valid:
    loop {
        print!("\nPlayer {}: ", c);
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut input).unwrap();
        input = input.trim().to_string();

        let result = treat_input(&input);
        match result {
            Some((a, b)) => {
                line = a;
                column = b;
                break;
            }
            None => println!("Invalid Input!"),
        }
    }
    return (line, column);
}

fn make_play(player: i32, pos: (i32, i32), board: &mut [[i32; 3]; 3]) {
    let l = pos.0 as usize;
    let c = pos.1 as usize;

    board[l][c] = player;
}

fn check_game_state(board: &[[i32; 3]; 3]) -> Winner {
    let mut sum_l: [i32; 3] = Default::default();
    let mut sum_c: [i32; 3] = Default::default();
    let mut d1 = 0;
    let mut d2 = 0;
    let mut draw = true;

    for i in 0..3 {
        sum_l[0] += board[0][i];
        sum_l[1] += board[0][i];
        sum_l[2] += board[0][i];

        sum_c[0] += board[i][0];
        sum_c[1] += board[i][1];
        sum_c[2] += board[i][2];

        d1 += board[i][i];
        d2 += board[i][2 - i];
    }

    for i in 0..3 {
        if sum_c[i] == 3 || sum_l[i] == 3 || d1 == 3 || d2 == 3 {
            return Winner::X;
        }
        if sum_c[i] == -3 || sum_l[i] == -3 || d1 == -3 || d2 == -3 {
            return Winner::O;
        }
    }

    for i in 0..3 {
        for j in 0..3 {
            if board[i][j] == 0 {
                return Winner::NONE;
            }
        }
    }
    return Winner::DRAW;
}

fn main() {
    let mut winner: Winner = Winner::NONE;
    let mut l: usize;
    let mut c: usize;
    let mut pos;
    let mut player: i32 = 1;
    let mut board: [[i32; 3]; 3] = Default::default();

    while winner == Winner::NONE {
        print_board(board);

        input_handler!(pos, board, player, get_input);
        make_play(player, pos, &mut board);

        winner = check_game_state(&board);

        player *= -1;
    }
    clear_screen!();
    print_board(board);
    println!("\nWinner: {:?}", winner);
}
