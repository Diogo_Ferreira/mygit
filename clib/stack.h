#ifndef STACK_H
#define STACK_H

#include <stdlib.h>

typedef enum
{
    TYPE_CHAR,
    TYPE_SHORT,
    TYPE_INT,
    TYPE_PTR
} Data_Type;

typedef struct
{
    Data_Type data_type;
    size_t size;
    int top;
    void *data;

} Stack;

#endif
