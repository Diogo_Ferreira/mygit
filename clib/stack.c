#include "stack.h"

Stack stack_create(int n_elements, Data_Type data_type)
{
    size_t size = 0;

    switch (data_type)
    {
    case TYPE_CHAR:
        size = n_elements * sizeof(char);
        break;
    case TYPE_SHORT:
        size = n_elements * sizeof(short);
        break;
    case TYPE_INT:
        size = n_elements * sizeof(int);
        break;
    case TYPE_PTR:
        size = n_elements * sizeof(void *);
        break;
    }

    Stack new = {
        .data_type = data_type,
        .size = size,
        .top = 0,
        .data = malloc(size)};
    return new;
}

void stack_free(Stack *stack)
{
    free(stack->data);
    stack->data = NULL;
}

void push(Stack *stack, void *n)
{
    stack->data[stack->top] = n;
}