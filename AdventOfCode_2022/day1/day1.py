def read_file():
    F = open("input.txt", "r")    
    calories = [[]]
    i = 0

    for number in F:
        if number == '\n':
            calories.append([])
            i += 1 
        else:
            calories[i].append(int(number))

    return calories



def p1(calories):
    max = 0

    for elve in calories:
        s = sum (elve)
        if s > max: 
            max = s

    return max

def p2(calories):
    total_cal = []

    for eleve in calories:
        total_cal.append(sum(eleve))
    
    total_cal.sort(reverse=True)
    return total_cal[0] + total_cal[1] + total_cal[2]



calories = read_file()
print("P1: ", p1(calories))
print("P2", p2(calories))

 
 

 
 
 

