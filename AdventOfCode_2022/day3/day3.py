def read_file():
    F = open("input.txt", "r")    
    input = []

    for line in F:
        input.append(line.replace('\n', ''))
    F.close()
    return input


def get_char_score(c):
    
    if c.isupper():
        return ord(c) - ord('A') + 27
    else: 
        return ord(c) - ord('a') + 1



def p1():
    input = read_file()
    char_list = []

    for i in range(ord('a'), ord('z') + 1):
        char_list.append(chr(i))
        char_list.append(chr(i - 32))


    for l in input:
        used_chars = {i : 0 for i in char_list}

        for c in l:
            used_chars[c] += 1
        print(used_chars)
        print("\n\n")

p1()





