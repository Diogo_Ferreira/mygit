play_value = {'X': 1, 'Y': 2, 'Z': 3}
round_value = {'A' : {'X': 3, 'Y': 6, 'Z': 0}, 'B': {'X': 0, 'Y': 3, 'Z': 6}, 'C': {'X': 6, 'Y': 0, 'Z': 3}}

def read_file():
    F = open("input.txt", 'r')
    plays = []

    for line in F:
        plays.append(line.replace('\n', '').split(' '))


    return plays



def get_round_score(play):
    return round_value[play[0]][play[1]] + play_value[play[1]]

def p1(plays):
    score = 0

    for x in plays:
        score += get_round_score(x)
    
    return score


def p2(plays):
    LOSE = 'X'
    DRAW = 'Y'
    WIN = 'Z'
    
    ROCK = 'X'
    PAPER = 'Y'
    SCISORS = 'Z'

    game = []

    what_to_play = {'A' : {LOSE: SCISORS, DRAW: ROCK, WIN: PAPER}, 'B': {LOSE: ROCK, DRAW: PAPER, WIN: SCISORS}, 'C': {LOSE: PAPER, DRAW: SCISORS, WIN: ROCK}}

    for line in plays:
        game.append([line[0], what_to_play[line[0]][line[1]]])
    
    return p1(game)

plays = read_file()

print("P1: ", p1(plays))
print("P2: ", p2(plays))
